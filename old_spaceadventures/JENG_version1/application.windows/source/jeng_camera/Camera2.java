package jeng_camera;

import processing.core.*;
import jeng_game.*;
import jeng_utilities.PairFloat;
import jeng_utilities.Timer;
import jeng_entity.EnHit;

public class Camera2 {
	
	public PairFloat pos = new PairFloat();
	public float angle = (float)Math.PI/2;
	public float zoom = 1;
	
	Timer time;
	PAppletb p;
	
	public Camera2(PAppletb ap)
	{
		p = ap;
		time = new Timer(p);
	}
	
	public String toString() { return "Camera2"; }
	
	private void applyTransformation()
	{
		
		
		p.scale(zoom);
		
		//translate to edge of screen
		p.translate(-pos.x , -pos.y );
		//translate to center of position
		p.translate(p.width/2/zoom, p.height/2/zoom);
		
		//rotate
		p.rotateAt((float)(angle - Math.PI/2), pos.x, pos.y);
		
		
		
		//p.rotate( angle );
		
	}
	
	public void feed()
	{	
		applyTransformation();
		time.update();
	}
	
	public void translate(float x, float y)
	{
		pos.x -= x;
		pos.y -= y;
	}
	
	public void move(float x, float y)
	{
		pos.x = x;
		pos.y = y;
	}
	
	public void follow(EnHit obj)
	{
		//move towards obj and speed proportional to distance
		pos.x -= (pos.x - obj.pos.x)*2*time.getSecPast();
		pos.y -= (pos.y - obj.pos.y)*2*time.getSecPast();
	}
	
	public void easeRot(float aangle)
	{
		angle -= (angle-aangle)*time.getSecPast();
	}
	
	public void easeZoom(float azoom)
	{
		zoom -= (zoom-azoom)*time.getSecPast();
	}
	
	public PairFloat getMousePos(int ax, int ay)
	{
		PairFloat m = new PairFloat(ax,ay);
		
		//reverse rotate
		//IMPLEMENT
		
		//reverese zoom
		m.x /= zoom;
		m.y /= zoom;
		
		//reverse translate
		m.x += pos.x - p.width/2/zoom;
		m.y += pos.y - p.height/2/zoom;
		
		return m;
	}
}

