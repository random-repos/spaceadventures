package jeng_entity_honey;

import java.util.Iterator;
import java.util.List;

import processing.core.PApplet;
import jeng_game.*;

public class Cell {
	
	MainGame p;
	Matrix<Cell> mtx;
	private List<Cell> lsFriends;
	
	public DNA color = new DNA();
	public DNA nextColor = new DNA();
	
	int x, y;
	
	boolean changed;
	
	public Cell( MainGame _p, Matrix<Cell> mtx_, int x_, int y_ ) { p = _p; x = x_; y = y_; mtx = mtx_; }
	
	public void init( List<Cell> lsFriends_ )
	{
		lsFriends = lsFriends_;
	}
	
	public void relax()
	{
		if(changed == false)
		{
			nextColor = new DNA();
		}
		
		changed = false;
	}
	
	public void research()
	{
		
		
		//averaging
		int nCounter = 0;
		int rc = 0;
		int gc = 0;
		int bc = 0;
		
		for(Iterator<Cell> it = lsFriends.iterator(); it.hasNext();)
		{
			nCounter++;
			
			Cell tempCell = it.next();
			
			rc += (tempCell.color.r) - (int)p.random(0,4) + 1;
			gc += (tempCell.color.g) - (int)p.random(0,4) + 1;
			bc += (tempCell.color.b) - (int)p.random(0,4) + 1;
		}
		
		rc /= nCounter;
		gc /= nCounter;
		bc /= nCounter;
		
		nextColor = new DNA(rc, gc, bc);
		
	}
	
	public void sell()
	{
		//if we have honey
		if(color.alive)
		{
			
			//if we have honey
			if(color.alive)
			{
				for(int j = -1; j <= 1; ++j)
					for(int i = -1; i <= 1; ++i)
					{
						if(i == 0 && j == 0)
							continue;
						
						//grab adjacent cell
						Cell c = mtx.at(i + x, j + y);
						
						//random based on iteration
						if( p.random(0,color.iteration) < 1)
						{
							if(c != null)
							{	
								//has changed
								c.changed = true;
								//copy color
								c.nextColor = color.copy();
								//increase the iteration
								c.nextColor.iteration = c.nextColor.iteration+1;
							}
							else
							{
								//createa  new cell
								Cell newCell = new Cell(p,mtx,i+x,j+y);
								newCell.changed = true;
								newCell.nextColor = color.copy();
								newCell.nextColor.iteration++;
								mtx.set(i+x, j+y, newCell);
							}
						}
						
						
						
							
					}
			}	
			
			/*
			for(Iterator<Cell> it = lsFriends.iterator(); it.hasNext();)
			{
				Cell tempCell = it.next();
				if( p.random(0,color.iteration) < 1)
				{
					//has changed
					tempCell.changed = true;
					//copy color
					tempCell.nextColor = color.copy();
					//increase the iteration
					tempCell.nextColor.iteration = tempCell.nextColor.iteration+1;
				}
			}
			*/
		}	
	}

	public void evolve()
	{
		color = nextColor.copy();
	}	
	
	public void kill(boolean alive_)
	{
		color.alive = false;
	}
	public void live()
	{
		color.alive = true;
	}
}
