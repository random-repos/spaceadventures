package jeng_entity_sprite;

import jeng_container.Message;
import jeng_entity.EnSprite;
import jeng_game.MainGame;
import jeng_utilities.Rectangle;

public class EnZafu extends EnSprite 
{
	//we are in contact with EnSpriteMy
	boolean isHit = false;
	
	public EnZafu(MainGame ap)
	{
		//need to fix this so it grabs child with attribute name = UP DOWN LEFT or RIGHT
		//currently just grabs the first thing
		super(ap, "zafu.xml", "sprite");
		myState = "DEFAULT";
	}
	
	public String toString() { return "EnZafu"; }
	
	public void update()
	{
		myState = "DEFAULT";
		//if we are not in contact
			//ishit = false;
	}
	
	public void sendMessage(Message amsg)
	{
		//we get a message saying it has been hit, set myState to POOF
		//set it back to not POOF
		
		//if we are not hit
		if(!isHit)
		{
			//we are hit
			isHit = true;
			
			myState = "POOF";
			
		}
	}
}
