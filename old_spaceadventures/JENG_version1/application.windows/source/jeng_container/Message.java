package jeng_container;

import jeng_entity.Entity;

public class Message {
	
	public Entity from;
	
	public String toString;
	public int toId = -1;
	
	Object msg;
	
	public Message(Entity pobj)
	{
		from = pobj;
	}
	
	public void sendTo(String aString, int aid)
	{
		toString = aString;
		toId = aid;
	}
	
	public int getFromId() { return from.getId(); };
	public String getFromString() { return from.toString(); };
	
}
