package jeng_container;

import processing.core.*;
import jeng_entity.*;
import jeng_entity_sprite.*;
import jeng_game.MainGame;
import java.util.Iterator;
import jeng_utilities.Rectangle;

//this container may only be sued by main thing
public class ColContainer extends Container {
	
	public ColContainer(MainGame ap)
	{
		super(ap);
		
		
	}
	
	public void update()
	{
		//update as usual
		super.update();
		
		//do collision
		//if collision type X
		if(true)
		{
			collision_01();
		}
	}
	
	//this routine detects collisions between EnSpriteMy and EnPlaque
	private void collision_01()
	{
		
		//go through entity list
		for(Iterator<Entity> it1 = enList.iterator(); it1.hasNext();)
		{
			Entity tempEn1 = it1.next();
			//check if it's a hit object
			if(tempEn1.toString() == "EnSpriteGroundMan")
			{
				for(Iterator<Entity> it2 = enList.iterator(); it2.hasNext();)
				{
					Entity tempEn2 = it2.next();
					if(tempEn2 instanceof EnHit && tempEn1 != tempEn2 )
					{
						operate_01((EnHit)tempEn1, (EnHit)tempEn2);
					}
				}
			}
		}
	}
	
	//assumes tempEn1 is a ground man
	private void operate_01(EnHit tempEn1, EnHit tempEn2)
	{
		//check for collision
		if( checkCollision((EnHit)tempEn1, (EnHit)tempEn2) )
		{
			//PApplet.println("WE'VE BEEN HIT!!!");
			//send message from collided object to EnSpriteGroundMan
			Message tempMessage = new Message(tempEn2);
			tempMessage.toString = "EnSpriteGroundMan";
			tempEn1.sendMessage(tempMessage);
		}
		
		//if zafu
		if(tempEn2 instanceof EnZafu)
		{
			//fade in and out the arrow
		}
			
		
		
	}
	
	
	private boolean checkCollision(Rectangle A, Rectangle B)
	{
		//debug
		/*
		p.stroke(1);
		p.strokeWeight(1);
		p.rect(A.x, A.y, A.w, A.h);
		p.rect(B.x, B.y, B.w, B.h);
		p.strokeWeight(1);
		p.noStroke();
		*/
		
		float leftA = A.x;
		float rightA = A.x + A.w;
		float topA = A.y;
		float bottomA = A.y + A.h;
		
		float leftB = B.x;
		float rightB = B.x + B.w;
		float topB = B.y;
		float bottomB = B.y + B.h;
		
		//check if sides are outside
		if( bottomA <= topB )
	        return false;
	    if( topA >= bottomB )
	        return false;    
	    if( rightA <= leftB )
	        return false;
	    if( leftA >= rightB )
	        return false;
	    
	    //no sides are outside -> collision return true
	    return true;
	}
	
	private boolean checkCollision(EnHit A, EnHit B)
	{
		return checkCollision(A.getHitRect(), B.getHitRect());
	}
	
}
