package jeng_entity_sprite;

import processing.core.PImage;
import jeng_entity.EnSprite;
import jeng_game.MainGame;
import jeng_utilities.*;

public class EnPainting extends EnSprite {

	PairFloat shakePos;
	//time the shake starts
	int shakeStart;
	//the last time we shook
	int lastShake;
	boolean isShake = false;
	
	public EnPainting(MainGame ap)
	{
		super(ap, "painting.xml", "sprite");
		shakePos = new PairFloat(pos.x, pos.y);
	}
	
	public String toString() { return "EnPainting"; }
	
	public void update()
	{
		if(p.myKeys.down && time.getTime() - shakeStart > 600)
		{
			shake();
		}
		
		if(time.getTime() - shakeStart > 300)
		{
			isShake = false;
			shakePos.x = pos.x;
			shakePos.y = pos.y;
		}
		
		if(isShake)
		{
			if(time.getTime()-lastShake > 35)
			{
				shakePos.x = pos.x + p.random( -5, 5 );
				shakePos.y = pos.y + p.random( -5, 5 );
			}
		}
		
		time.update();
	}
	
	public void draw()
	{
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		p.image( tempImage, shakePos.x, shakePos.y );
	}
	
	public void shake()
	{
		isShake = true;
		shakeStart = time.getTime();
	}
}
