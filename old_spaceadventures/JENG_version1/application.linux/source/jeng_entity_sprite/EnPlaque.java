package jeng_entity_sprite;

import jeng_container.Message;
import jeng_game.MainGame;

public class EnPlaque extends EnSpriteStatic {
	public EnPlaque(MainGame ap)
	{
		super(ap, "painting.xml");		
		Message tempMsg = new Message(this);
		tempMsg.toString = "EnSpriteMy";
		msgStack.push(tempMsg);
	}
	
	public String toString() { return "EnPlaque"; }
}
