package jeng_entity_sprite;

import processing.core.*;
import jeng_utilities.*;
import jeng_container.Container;
import jeng_container.Message;
import jeng_entity.EnSprite;
import jeng_game.MainGame;


public class EnSpriteGroundMan extends EnSprite{
	
	//direction variables
	//direction angle in radians
	double angle = 0;
	// left
	boolean dir = true;
	
	boolean isCollide = false;
	
	//container variables
	Container myCont;
	
	public EnSpriteGroundMan(MainGame ap)
	{
		super(ap, "testxml.xml", "sprite");
		
		//set up container with an arrow
		myCont = new Container(this,ap);
		
		//EnArrow tempEntity = new EnArrow(ap, this, 0, "HOLD");
		//myCont.addEntity(tempEntity);
	}
	
	public String toString() { return "EnSpriteGroundMan"; };
	
	private void handleInput()
	{

		myState = "STAND";
		
		if( p.myKeys.left )
		{
			if(dir == false)
			{
				angle = Math.PI;
				myState = "RUN";
			}
			//set left
			dir = false;
		}
		
		if(p.myKeys.right)
		{
			if(dir == true)
			{
				angle = 0;
				myState = "RUN";
			}
			
			dir = true;
		}
		
		//if collision
		if(isCollide && myState != "RUN")
		{
			if(p.myKeys.down)
			{
				myState = "CROUCH";
			}
		}
		else
		{		
			if(p.myKeys.down)
			{
				angle = Math.PI/2;
				myState = "RUN";
				if(p.myKeys.right)
					angle = Math.PI/4;
				else if(p.myKeys.left)
					angle = 3*Math.PI/4;
			}			
		}
		
		if(p.myKeys.up)
		{
			angle = -Math.PI/2;
			myState = "RUN";
			
			
			if(p.myKeys.right)
				angle = -Math.PI/4;
			else if(p.myKeys.left)
				angle = -3*Math.PI/4;
		}
	}

	public void update()
	{
		//handle the input
		handleInput();
		//assume there was no collision
		isCollide = false;
		
		//update position like normal
		pos.x += (float)Math.cos((float)angle)*myFrame.velocity*time.getSecPast()/2;
		pos.y += (float)Math.sin((float)angle)*myFrame.velocity*time.getSecPast()/5;
		
		//update child objects
		myCont.update();

		//update timer
		time.update();
	}
	
	public void sendMessage(Message amsg)
	{
		//we gets a message -> collision
		if(amsg.from.toString() == "EnZafu")
		{
			isCollide = true;
		}
		
	}
	
	public void draw()
	{
		
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		//tint test
		//p.tint(127, 10);
				
		p.pushMatrix();
		p.smooth();
		//if direction is RIGHT
		if(dir)
		{
			p.image
			(
				tempImage,
				pos.x, 
				pos.y
			);
			
		} else 
		{
			p.pushMatrix();
			
			p.scale( (float)-1.0, (float)1.0);
			p.image
			(
				tempImage,
				-pos.x - myFrame.myDrawBox.w, 
				pos.y
			);
			p.popMatrix();
			
		}
		p.noSmooth();
		p.popMatrix();
		
		//TO DRAW RELATIVE WE SHOULD
		//pushMatrix
		//make image location our origin
		//draw self
		//draw children
		//draw container relative
		//pop matrix
		//draw stuff in container absolute
		myCont.draw();
	}
	
	
	public PairFloat getImageCenter(PairFloat apos)
	{
		return new PairFloat(apos.x + myFrame.myDrawBox.w/2, apos.y + myFrame.myDrawBox.h/2 + 20);
	}
	public PairFloat getImageCenter()
	{
		return new PairFloat(pos.x + myFrame.myDrawBox.w/2, pos.y + myFrame.myDrawBox.h/2 + 20);
	}
	
	public void moveCenter(PairFloat apos)
	{
		pos.x = apos.x - myFrame.myDrawBox.w/2;
		pos.y = apos.y - myFrame.myDrawBox.h/2 + 20;
	}
}

