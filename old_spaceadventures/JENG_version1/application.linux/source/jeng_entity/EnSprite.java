package jeng_entity;
import processing.core.PImage;
import processing.xml.XMLElement;
import jeng_game.MainGame;
import jeng_graphics.SpriteFrame;
import jeng_graphics.SpriteGraph;
import jeng_utilities.PairFloat;
import jeng_utilities.Rectangle;
import jeng_utilities.Timer;

public class EnSprite extends EnHit
{
	protected Timer time;
	
	//sprite
	protected SpriteGraph mySg;
	protected SpriteFrame myFrame;
	protected String myState;
	
	public EnSprite(MainGame ap)
	{
		super(ap);
		time = new Timer(p);
	}
	
	//sets up sprite graph using xml data from XMLFile
	public EnSprite(MainGame ap, String XMLFile, String aname)
	{
		super(ap);
		
		time = new Timer(ap);
		
	
		//load the file
		mySg = new SpriteGraph(ap);
		XMLElement myXML = new XMLElement(ap,XMLFile);
		
		//set up anim graph
		myXML = myXML.getChild(aname);
		mySg.setGraph(myXML);
		
		//grab the first frame
		myFrame = mySg.grabFrame("DEFAULT");
		
		//set up hit box default
		hitRect.set(myFrame.myDrawBox.x, myFrame.myDrawBox.y, myFrame.myDrawBox.w, myFrame.myDrawBox.h);
		
	}
	
	public String toString() 
	{ 
		return "EnSprite"; 
	}
	
	public PairFloat getImageCenter()
	{
		return new PairFloat(pos.x + myFrame.myDrawBox.w/2,pos.y + myFrame.myDrawBox.h/2);
	}
	
	public Rectangle getHitRect()
	{
		hitRect.x = pos.x;
		hitRect.y = pos.y;
		hitRect.w = myFrame.myDrawBox.w;
		hitRect.h = myFrame.myDrawBox.h;
		return hitRect;
	}
	
	//override me
	public void update()
	{		

	}
	
	public void draw()
	{
		//grab image data
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		//draw the image
		p.image( tempImage, pos.x, pos.y );
	}
	
}
