package jeng_entity;

import jeng_game.MainGame;
import jeng_utilities.*;

public class EnHit extends Entity 
{
	//position
	public PairFloat pos = new PairFloat(500,500);
	protected Rectangle hitRect = new Rectangle();
	
	public EnHit(MainGame ap)
	{
		super(ap);
	}
	
	//i.e. teleport fcn
	public void setPosition( float x, float y )
	{
		pos.x = x;
		pos.y = y;
	}
	
	//this function needs to be overriden to return the current position of a moving object
	public Rectangle getHitRect()
	{
		return hitRect;
	}

}
