package jeng_graphics;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import jeng_utilities.BoxInt;

import processing.core.*;


public class SpriteFrame {
	
	public int id;
	public int time;
	String state;
	
	//bonus data
	public int velocity;
	
	//draw box
	public BoxInt myDrawBox;
	//map to other frames
	Map<String, SpriteFrame> myMap = new HashMap<String, SpriteFrame>();
	//list of hit boxes, relative to 0,0
	List<BoxInt> myHitBox = new LinkedList<BoxInt>();
	
	//image
	public PImage myImage;
	
	public SpriteFrame(){};	
	
	//setup functions
	public void addFrame(SpriteFrame aframe, String astate)
	{
		myMap.put(astate, aframe);
	};
	
	public void setVars(String astate, int aid, int atime)
	{
		state = astate;
		id = aid;
		time = atime;
	}
	
	//some function to set up draw box
	public void setDrawBox(BoxInt abox)
	{
		myDrawBox = abox;
	}
	
	//some function to set up hit box list
	public void addHitBox(BoxInt abox)
	{	
		myHitBox.add(abox);
	}
	
	//some function to set up image reference
	public void setImage(PImage aimage)
	{
		myImage = aimage;
	}
	
	
	//usage methods
	public SpriteFrame getFrame(String astate)
	{
		//if empty, return yourself
		if(myMap.isEmpty())
			return this;
		else if(myMap.get(astate) == null)
			return this.myMap.get("DEFAULT");
		else
			return this.myMap.get(astate);
	}
}
