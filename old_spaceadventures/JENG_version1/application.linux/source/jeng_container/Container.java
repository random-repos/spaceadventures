package jeng_container;
import processing.core.*;
import java.util.List;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Iterator;

import jeng_entity.Entity;
import jeng_game.MainGame;

//make this extend entity?

public class Container extends Entity {

	//parent entity
	Entity en = null;
	
	//list of objects we have
	List<Entity> enList = new LinkedList<Entity>();
	//our message stack
	Stack<Message> msgStack = new Stack<Message>();

	//
	public Container(Entity aen, MainGame ap)
	{
		super(ap);
		en = aen;
	}
	public Container(MainGame ap)
	{
		super(ap);
	}
	
	//adds an object
	public void addEntity(Entity aentity)
	{
		enList.add(aentity);
	}
	
	public Entity getLast()
	{
		return enList.get(enList.size()-1);
	}
	
	public Entity getEntity(String name)
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
		{
			Entity temp = it.next();
			if(temp.toString() == name)
				return temp;
		}
		return null;
	}
	
	//draws everything
	public void draw()
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
			it.next().draw();
	}
	
	//updates everything
	public void update()
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
			it.next().update();
	}
	
	//handles all msgs
	public Message getMessage()
	{
		
		//THIS HANDLES MESSAGES OF ALL CHILD OBJECTS
		//MAYBE MAKE THIS FUNCTION SEPERATE FROM getMessage
		
		//check for new msgs using get msg fcn
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
		{
			
			Entity temp = it.next();
			//while there is msgs
			while(temp.isMsg())
			{
				//get a msg
				Message tempMsg = temp.getMessage();
				//see where it's going to
				//if its going to somewhere in our list
					//send it there
				for(Iterator<Entity> it2 = enList.iterator(); it2.hasNext();)
				{
					Entity temp2 = it2.next();
					if(tempMsg.toString == temp2.toString())
					{
						if(tempMsg.toId == -1)
						{
							//send the msg
							temp2.sendMessage(tempMsg);
						}
						else if(tempMsg.toId == temp2.id)
						{
							//send the msg
							temp2.sendMessage(tempMsg);
							break;
						}
					}
				}
				
				//NOT IMPLEMENTED
				//else
					//return msg i.e. send it up a level
			}
		}
		
		//relay the msgs to correct objects using send msg
		
		//send msgs up a level if no place to relay
		
		return null;
		
	}
	
	public boolean isMsg()
	{
		//check if there are any messages to relay up a level
		//for now
		return msgStack.isEmpty();
	}
	
	public void sendMessage(Message amsg)
	{
		
	}
	
	public void deleteAll()
	{
		enList.clear();
	}
	
}
