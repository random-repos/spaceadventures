package jeng_game;
import processing.core.*;

public class PAppletb extends PApplet{

	public void rotateAt(float angle, float ax, float ay)
	{
		translate(ax, ay);
		rotate(angle);
		translate(-ax, -ay);
	}
}
