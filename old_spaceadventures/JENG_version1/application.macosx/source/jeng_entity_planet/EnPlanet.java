package jeng_entity_planet;

import jeng_container.Message;
import jeng_entity.Entity;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;
import jeng_utilities.Timer;

//enPlanet has its own special hit detection that is handled in EnSpriteMy class directly
public class EnPlanet extends Entity {
	//timer
	Timer time;
	//position
	public PairFloat pos = new PairFloat(900,500);
	public float radius = 300;
	public float mass = 400;
	
	public EnPlanet(MainGame ap)
	{
		super(ap);
		
		time = new Timer(ap);
		
		//send msg to EnSpriteMy
		Message tempMsg = new Message(this);
		tempMsg.toString = "EnSpriteMy";
		msgStack.push(tempMsg);
		
		//send msg to EnSpriteMy
		tempMsg = new Message(this);
		tempMsg.toString = "EnSpriteMoonMan";
		msgStack.push(tempMsg);
		

	}
	
	public String toString() {	return "EnPlanet"; }
	
	//i.e. teleport fcn
	public void setPosition( float x, float y )
	{
		pos.x = x;
		pos.y = y;
	}
	
	public void draw()
	{
		
		
		
		
		
		p.pushMatrix();
		/*
		p.translate(pos.x, pos.y, 0);
		p.rotateX(p.random(50));
		p.rotateZ(p.random(50));
		p.rotateY(p.random(50));
		*/
		
		p.noStroke();
		//p.lights();
		
		//p.sphereDetail(10);
		//p.sphere(radius);
		
		//p.noLights();
		//p.tint(0,255);
		p.fill(230);
		//p.noFill();
		
		p.stroke(2);		
		p.ellipse(pos.x,pos.y,radius*2,radius*2);
		//p.ellipse(pos.x, pos.y, 5, 5);
		p.noStroke();
		p.noFill();
		//p.noTint();
		
		p.popMatrix();
		
		
	}
	
	public void update()
	{
		//spin, animate, do some fancy shit
		
		
		time.update();
	}
	
	
}
