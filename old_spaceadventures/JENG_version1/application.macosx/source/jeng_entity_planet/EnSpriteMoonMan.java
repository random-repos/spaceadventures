package jeng_entity_planet;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import processing.core.*;
import jeng_game.MainGame;
import jeng_container.Message;
import jeng_entity.EnSprite;
import jeng_utilities.LineFcn;
import jeng_utilities.PairFloat;

public class EnSpriteMoonMan extends EnSprite {
	
	//facing right
	boolean dir = true;
	
	//current velocity
	PairFloat vel = new PairFloat();
	
	//angle at which we are
	public double rotAngle;
	//angle at which we draw
	double drawAngle;
	
	//have we collided with anything?
	boolean isCollide = false;
	
	class ProjPos
	{
		public PairFloat end = new PairFloat();
		public PairFloat endPlanet;
		public float angle;
		public float time;
	}	
	
	//projected position, angle and time
	boolean isProj = false; //have we projected landing yet?
	ProjPos projPos = new ProjPos();
	EnPlanet planetHit = null;
	
	int RADIUS = 47;
	double GCONST = 500000;
	float timeRes = 20; //resolution of approximation in ms
	List<EnPlanet> planetList = new LinkedList<EnPlanet>();
	
	boolean isJump = false;
	
	public EnSpriteMoonMan(MainGame ap)
	{
		super(ap, "testxml3.xml", "sprite");
	}
	
	public String toString() { return "EnSpriteMoonMan"; }
	
	//adds velocity x, y relative to rot angle	
	private void addVelocity(float ax, float ay)
	{
		vel.y += ax*Math.cos(rotAngle - Math.PI) - ay*Math.sin(rotAngle  - Math.PI);
		vel.x += ax*Math.sin(rotAngle  - Math.PI) + ay*Math.cos(rotAngle  - Math.PI);		
	}
	
	//adds friction with c and constant of friction
	//c between 0 and 1
	//this function is shit
	private void addFriction(float c)
	{
		PairFloat ff = new PairFloat();
		ff.x = vel.x*c;
		ff.y = vel.y*c;
		
		//LineFcn.getLength(vel)*c
		
		//check if we want to make it 0
		if(vel.x < time.getSecPast()*ff.x)
			vel.x = 0;
		else
			vel.x -= time.getSecPast()*ff.x;
		
		//check if we want to make it 0
		if(vel.y < time.getSecPast()*ff.y)
			vel.y = 0;
		else
			vel.y -= time.getSecPast()*ff.y;
	}
	
	//handles input
	private void planetInput()
	{
	
		if(p.myKeys.down)
		{
			myState = "CROUCH";
		}
		
		if(p.myKeys.left)
		{
			dir = false;
			myState = "RUN";
		}
		if(p.myKeys.right)
		{
			dir = true;
			myState = "RUN";
		}
		
		if(p.myKeys.up)
		{
			myState = "JUMP";
			//increase y vel relative to rotAngle
			addVelocity(0, -850);
		}
	}
	
	private ProjPos calcLand(PairFloat apos, PairFloat velVect, int iteration)
	{
		
		if(iteration > 1000)
		{
			ProjPos projected = new ProjPos();
			projected.end.x = apos.x;
			projected.end.y = apos.y;
			
			//special angle
			projected.angle = 1111;
			
			projected.time = (float)iteration*timeRes;
			//PApplet.println("cheese");
			return projected;
		}
		
		for(Iterator<EnPlanet> it = planetList.iterator(); it.hasNext();)
		{
			EnPlanet tp = it.next();
			//check collision
			if( LineFcn.getLength(apos,tp.pos) <= RADIUS + tp.radius )
			{
				//PApplet.println("notcheese");
				ProjPos projected = new ProjPos();
				projected.end = apos;
				projected.endPlanet = tp.pos;
				projected.angle = LineFcn.getAngle(tp.pos, apos);
				
				
				projected.time = iteration*timeRes;
				
				if(projected.time > 3000 || isProj )
				{
					isProj = true;
					projected.angle += 2*(float)Math.PI;
				}
				
				return projected;
				
			}				
		}
		
		//add up force vectors for each planet
		PairFloat av = ESMMFCN.getAccelVector(this, apos);
		velVect.x += av.x*timeRes/1000;
		velVect.y -= av.y*timeRes/1000;
		
		//make the recusive call
		return calcLand(new PairFloat(apos.x + velVect.x*timeRes/1000, apos.y + velVect.y*timeRes/1000), velVect, iteration+1);
	}
	
	public void update()
	{
		//regular friction
		//addFriction(2);
		
		//if we collided get input
		if(isCollide)
		{
			//super friction
			vel.x = vel.y = 0;
			//addFriction(25);
			
			//we no longer know where we will land
			//isProj = false;
			//the we want to land
			myState = "STAND";
			//grab input
			planetInput();
			
			//calculate rotation for initial movement
			rotAngle = -LineFcn.getAngle(planetHit.pos, getImageCenter());
			
			//get velocity vectors from default movement and move
			if(dir)
				addVelocity( -1*myFrame.velocity*(float)Math.cos(.5), -1*myFrame.velocity*(float)Math.sin(.5) );
			else
				addVelocity( 1*myFrame.velocity*(float)Math.cos(.5), -1*myFrame.velocity*(float)Math.sin(.5) );
		}
		else
		{
			//if(myState != "RUN")
			//	myState = "JUMP";
		}
		
		
		//add gravity
		PairFloat av = ESMMFCN.getAccelVector(this);
		
		//add gravitational force vectors to regular velocity vectors
		vel.x += av.x*time.getSecPast();
		vel.y -= av.y*time.getSecPast();
		
		//move
		pos.x += vel.x*time.getSecPast();
		pos.y += vel.y*time.getSecPast();
		
		/*
		p.stroke(100);
		p.strokeWeight(1);
		p.line(getImageCenter().x, getImageCenter().y, getImageCenter().x+vel.x, -getImageCenter().y-vel.y);
		p.stroke(0);
		p.line(getImageCenter().x, getImageCenter().y, getImageCenter().x+av.x, -getImageCenter().y-av.y);
		*/
		
		
		//now test for collision
		for(Iterator<EnPlanet> it = planetList.iterator(); it.hasNext();)
		{
			//assume no collision
			isCollide = false;
			
			EnPlanet tp = it.next();
			
			//check if we have collision
			if( LineFcn.getLength(getImageCenter(),tp.pos) <= RADIUS + tp.radius )
			{
				//we've been hit!
				isCollide = true;
				planetHit = tp;
				isProj = false;
				if(myState == "JUMP")
					myState = "RUN";
			
				//quit the loop, we aren't going to hit anything else
				break;
			}
		}
		
		//if there is deep contact
		if(isCollide && LineFcn.getLength(getImageCenter(),planetHit.pos) <= RADIUS + planetHit.radius)
		{
			
			//debug goodies
			
			/*
			p.stroke(100);
			p.line(getImageCenter().x, getImageCenter().y, planetHit.pos.x, planetHit.pos.y);
			p.ellipse(getImageCenter().x, getImageCenter().y, 47*2, 47*2);
			p.noStroke();
			*/
			
			
			//calculate rotation after movement
			rotAngle = -LineFcn.getAngle(planetHit.pos, getImageCenter());
			
			moveCenter( new PairFloat(
					planetHit.pos.x + (float) ( Math.cos(rotAngle) * (RADIUS + planetHit.radius ) ),
					planetHit.pos.y - (float) ( Math.sin(rotAngle) * (RADIUS + planetHit.radius ) )
					));
			
			//calculate display angle
			drawAngle = LineFcn.getAngle(planetHit.pos, getImageCenter()) + Math.PI/2;
		}
		else
		{
			//project
			projPos = calcLand(getImageCenter(),vel.clone(),1);
			
			//addition:
			//make draw angle rotate at a rate inversely proportional to the velocity
			if(	projPos.angle != 1111 
				&& projPos.time > 200 
				&& planetHit != null && LineFcn.getLength(pos,planetHit.pos) > planetHit.radius + RADIUS + 30)
			{
				drawAngle -= (drawAngle-projPos.angle - Math.PI/2)*time.getSecPast()*1100/projPos.time;
				myState = "JUMP";
			}
			
			//if not projected
			//evidently, we don't need this
			//isProj = true;
		}
		
		time.update();		
	}
	
	public void draw()
	{
		
		
		//get frame
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		
		p.pushMatrix();
		
		p.rotateAt((float)drawAngle, getImageCenter().x, getImageCenter().y);
		
		p.smooth();
		//if direction is RIGHT
		if(dir)
		{
			p.image
			(
				tempImage,
				pos.x, 
				pos.y
			);
			
		} else 
		{
			p.pushMatrix();
			
			p.scale( (float)-1.0, (float)1.0);
			p.image
			(
				tempImage,
				-pos.x - myFrame.myDrawBox.w, 
				pos.y
			);
			p.popMatrix();
			
		}
	
		
				
		p.noSmooth();
		
		p.popMatrix();
		
		
		
		//p.stroke(0);
		//p.ellipse(projPos.end.x, projPos.end.y, 100, 100);
	}
	
	public void sendMessage(Message amsg)
	{
		//if planet, then add it to our list and return
		if(amsg.from.toString() == "EnPlanet")
		{
			planetList.add((EnPlanet)amsg.from);
			return;
		}
	}
	
	
	//bonus functions
	
	//returns the center of an image at apos
	public PairFloat getImageCenter(PairFloat apos)
	{
		//return new PairFloat(apos.x + myFrame.myDrawBox.w/2, apos.y + myFrame.myDrawBox.h/2);
		return new PairFloat(apos.x + myFrame.myDrawBox.w/2, apos.y + myFrame.myDrawBox.h/2 + 20);
	}
	
	//returns the center of the image
	public PairFloat getImageCenter()
	{
		//return new PairFloat(pos.x + myFrame.myDrawBox.w/2, pos.y + myFrame.myDrawBox.h/2);
		return new PairFloat(pos.x + myFrame.myDrawBox.w/2, pos.y + myFrame.myDrawBox.h/2 + 20);
	}
	
	
	//moves the center to position apos
	private void moveCenter(PairFloat apos)
	{
		pos.x = apos.x - myFrame.myDrawBox.w/2;
		pos.y = apos.y - myFrame.myDrawBox.h/2 - 20;
	}
}
