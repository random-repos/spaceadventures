package jeng_entity_planet;

import java.util.Iterator;

import jeng_utilities.LineFcn;
import jeng_utilities.PairFloat;

public class ESMMFCN {
	public static PairFloat getAccelVector(EnSpriteMoonMan sp)
	{
		return getAccelVector(sp,sp.pos);
	}
	public static PairFloat getAccelVector(EnSpriteMoonMan sp, PairFloat apos)
	{
		PairFloat av = new PairFloat(0,0);
		for(Iterator<EnPlanet> it = sp.planetList.iterator(); it.hasNext();)
		{
			//wrong wrong wrong!
			EnPlanet tp = it.next();
			float mag = (float)sp.GCONST * tp.mass / LineFcn.getSquare(LineFcn.getLength(sp.getImageCenter(apos), tp.pos));
			//float mag = (float)5000 * tp.mass /(LineFcn.getLength(sp.getImageCenter(apos), tp.pos));
			float xf = (float)Math.cos(LineFcn.getAngle(sp.getImageCenter(apos), tp.pos)) * mag;
			float yf = (float)Math.sin(LineFcn.getAngle(sp.getImageCenter(apos), tp.pos)) * mag;
			
			//add the forces to our velocity 
			av.x += xf;
			av.y -= yf;
		}
		return av;
	}
	
}
