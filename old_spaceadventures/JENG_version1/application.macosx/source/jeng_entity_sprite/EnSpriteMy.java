package jeng_entity_sprite;
import processing.core.*;
import jeng_utilities.*;
import jeng_container.Container;
import jeng_container.Message;
import jeng_entity.EnSprite;
import jeng_entity_planet.EnPlanet;
import jeng_game.MainGame;

import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

public class EnSpriteMy extends EnSprite 
{
	
	//direction vector
	//unused
	
	
	//direction variables
	//direction angle in radians
	double angle = 0;
	// left
	boolean dir = true;
	
	
	
	//for planet mode
	PairFloat vel = new PairFloat(0,0);
	boolean planetMode = true;
	boolean inAir = false;
	int RADIUS = 67;
	double GCONST = 500000;
	double rotAngle = 0;
	float timeRes = 1/50; //resolution of approximation in ms
	List<EnPlanet> planetList = new LinkedList<EnPlanet>();
		
	//container variables
	Container myCont;
	
	public EnSpriteMy(MainGame ap)
	{
		super(ap, "testxml.xml", "sprite");
		
		//set up container with an arrow
		myCont = new Container(this,ap);
		
		EnArrow tempEntity = new EnArrow(ap, 0, "HOLD");
		myCont.addEntity(tempEntity);
		
		vel.y -= -100;
	}
	
	public String toString() { return "EnSpriteMy"; };
	
	private void planetInput()
	{

		//vel.setZero();
		
		if(inAir)
			myState = "JUMP";
		else if(p.myKeys.up)
		{
			inAir = true;
			vel.y -= 200;
		}
	}
	
	private void genericInput()
	{
		myState = "STAND";
		
		if( p.myKeys.left )
		{
			if(dir == false)
			{
				angle = Math.PI;
				myState = "RUN";
			}
			//set left
			dir = false;
		}
		
		if(p.myKeys.right)
		{
			if(dir == true)
			{
				angle = 0;
				myState = "RUN";
			}
			
			dir = true;
		}
		
		if(false)
		{
			if(p.myKeys.down)
			{
				angle = Math.PI/2;
				myState = "RUN";
				if(p.myKeys.right)
					angle = Math.PI/4;
				else if(p.myKeys.left)
					angle = 3*Math.PI/4;
			}
		}
		else
		{		
			//else if there is collision of type SOMETHING
			//i.e. if we got a MSG
			if(p.myKeys.down)
			{
				myState = "CROUCH";
			}
			
			
		}
	}
	
	private void standardInput()
	{
		if(p.myKeys.up)
		{
			angle = -Math.PI/2;
			myState = "RUN";
			
			
			if(p.myKeys.right)
				angle = -Math.PI/4;
			else if(p.myKeys.left)
				angle = -3*Math.PI/4;
		}
	}
	
	private void handleInput()
	{

		
		
		if(!planetMode)
		{
			genericInput();
			standardInput();
		}
		
		if(planetMode)
			planetInput();
	}
	
	class ProjPos
	{
		public PairFloat end;
		public PairFloat endPlanet;
		public float angle;
		public float time;
	}
	
	private ProjPos calcLand(PairFloat apos, PairFloat velVect, int iteration)
	{
		
		if(iteration > 1000)
		{
			ProjPos projected = new ProjPos();
			projected.time = (float)iteration*timeRes;
			//PApplet.println("cheese");
			return projected;
		}
		
		for(Iterator<EnPlanet> it = planetList.iterator(); it.hasNext();)
		{
			EnPlanet tp = it.next();
			//check collision
			//this is wrong, we can't use getImageCenter as position.
			if( LineFcn.getLength(getImageCenter(apos),tp.pos) <= RADIUS + tp.radius )
			{
				PApplet.println("notcheese");
				ProjPos projected = new ProjPos();
				projected.end = apos;
				projected.endPlanet = tp.pos;
				projected.angle = LineFcn.getAngle(tp.pos, apos);
				projected.time = iteration*timeRes;
				return projected;
				
			}				
		}
		
		//add up force vectors for each planet
		PairFloat av = EnSpriteMyFcn.getAccelVector(this,apos);
		velVect.x += av.x;
		velVect.y += av.y;
		
		/*
		for(Iterator<EnPlanet> it = planetList.iterator(); it.hasNext();)
		{
			EnPlanet tp = it.next();
			float mag = GCONST * tp.mass / LineFcn.getSquare(LineFcn.getLength(getImageCenter(), tp.pos)); 
			float xf = (float)Math.cos(LineFcn.getAngle(getImageCenter(), tp.pos))*mag;
			float yf = (float)Math.sin(LineFcn.getAngle(getImageCenter(), tp.pos))*mag;
			
			//add the forces to our velocity 
			velVect.x += xf * timeRes;
			velVect.y += yf * timeRes;
		}*/
		
		//PApplet.print(apos.x + velVect.x*timeRes);
		//PApplet.print(" ");
		//PApplet.println(apos.y + velVect.y*timeRes);
		
		//make the recusive call
		return calcLand(new PairFloat(apos.x + velVect.x*timeRes, apos.y + velVect.y*timeRes), velVect, iteration+1);
	}
	
	public void update()
	{
		//handle the input
		handleInput();
		
		if(!planetMode)
		{			
			//update position like normal
			pos.x += (float)Math.cos((float)angle)*myFrame.velocity*time.getSecPast()/2;
			pos.y += (float)Math.sin((float)angle)*myFrame.velocity*time.getSecPast()/5;
			
		}
		else
		{
			planetUpdate();
		}
		
		//update child objects
		myCont.update();

		//update timer
		time.update();
	}
	
	public void planetUpdate()
	{
		//calculate gravitational force vector
		PairFloat av = EnSpriteMyFcn.getAccelVector(this);
		
		if(inAir)
		{
			//add gravitational force vectors to regular velocity vectors
			vel.x += av.x*time.getSecPast();
			vel.y += av.y*time.getSecPast();
			
			//move
			pos.x += vel.x*time.getSecPast();
			pos.y += vel.y*time.getSecPast();
		}
		
		
		//assume we are in air
		//inAir = true;
		//see if we hit anything
		for(Iterator<EnPlanet> it = planetList.iterator(); it.hasNext();)
		{
			EnPlanet tp = it.next();
			//check collision
			if( LineFcn.getLength(getImageCenter(),tp.pos) <= RADIUS + tp.radius )
			{
				//we've been hit!
				//calculate norm angle
				vel.x = vel.y = 0;
				
				//calculate rotation for initial movement
				rotAngle = -LineFcn.getAngle(tp.pos, pos) -Math.PI/2 + Math.PI/7 ;
				
				//get velocity vectors from default movement and move
				vel.x += Math.cos(rotAngle) * myFrame.velocity;
				vel.y += Math.sin(rotAngle) * myFrame.velocity;
				
				pos.x += vel.x*time.getSecPast();
				pos.y += vel.y*time.getSecPast();
			
				//calculate rotation again
				rotAngle = -LineFcn.getAngle(tp.pos, pos);
				
				//move him to surface at angle
				moveCenter( new PairFloat(
						tp.pos.x + (float) ( Math.cos(rotAngle) * (RADIUS + tp.radius) ),
						tp.pos.y + (float) ( Math.sin(rotAngle) * (RADIUS + tp.radius) )
						));
				
				//calculate rotation angle one more time
				rotAngle = -Math.PI/2 + Math.PI/7;
				
				
				//set velocity back to zero
				vel.x = vel.y = 0;
				//we are on ground
				inAir = false;
				//quit the loop, we aren't going to hit anything else
				break;
			}
		}
					
		//calculate landing place
		//ProjPos pp = calcLand(new PairFloat(pos.x + vel.x*time.getSecPast(), pos.y + vel.y*time.getSecPast()), new PairFloat(vel.x, vel.y),0);
				
		
	}
	
	public void sendMessage(Message amsg)
	{
		/*
		if(amsg.from.toString() == "EnPlaque")
		{
			collideList.add((EnSpriteStatic)amsg.from);
			return;
		}
		*/
		
		//if planet, then add it to our list and return
		if(amsg.from.toString() == "EnPlanet")
		{
			planetList.add((EnPlanet)amsg.from);
			return;
		}
	}
	
	public void draw()
	{
		//p.rect(pos.x, pos.y, 50, 50);
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		//tint test
		//p.tint(127, 10);
				
		p.pushMatrix();
		
		//translate about point (turn this into a function christ)
		p.translate(getImageCenter().x, getImageCenter().y);
		p.rotate((float)rotAngle);
		p.translate(-getImageCenter().x, -getImageCenter().y);
		
		p.smooth();
		//if direction is RIGHT
		if(dir)
		{
			p.image
			(
				tempImage,
				pos.x, 
				pos.y
			);
			
		} else 
		{
			p.pushMatrix();
			
			p.scale( (float)-1.0, (float)1.0);
			p.image
			(
				tempImage,
				-pos.x - myFrame.myDrawBox.w, 
				pos.y
			);
			p.popMatrix();
			
		}
		p.noSmooth();
		p.popMatrix();
		
		//TO DRAW RELATIVE WE SHOULD
		//pushMatrix
		//make image location our origin
		//draw self
		//draw children
		//draw container relative
		//pop matrix
		//draw stuff in container absolute
		myCont.draw();
	}
	
	
	public PairFloat getImageCenter(PairFloat apos)
	{
		return new PairFloat(apos.x + myFrame.myDrawBox.w/2, apos.y + myFrame.myDrawBox.h/2 + 20);
	}
	public PairFloat getImageCenter()
	{
		return new PairFloat(pos.x + myFrame.myDrawBox.w/2, pos.y + myFrame.myDrawBox.h/2 + 20);
	}
	
	public void moveCenter(PairFloat apos)
	{
		pos.x = apos.x - myFrame.myDrawBox.w/2;
		pos.y = apos.y - myFrame.myDrawBox.h/2 + 20;
	}
}
