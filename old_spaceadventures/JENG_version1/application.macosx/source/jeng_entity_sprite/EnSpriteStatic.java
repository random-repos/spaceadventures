package jeng_entity_sprite;

import processing.core.PImage;
import jeng_entity.EnSprite;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;

public class EnSpriteStatic extends EnSprite{

	public EnSpriteStatic(MainGame ap, String filename)
	{
		super(ap, filename, "sprite");		
	}
	
	public String toString() { return "EnSpriteStatic"; }
	
	public void update()
	{
		time.update();
	}
	
	public void draw()
	{
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		p.image( tempImage, pos.x, pos.y );
	}
}
