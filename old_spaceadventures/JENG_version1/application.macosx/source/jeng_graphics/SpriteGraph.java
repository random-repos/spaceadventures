package jeng_graphics;

import processing.core.*;
import processing.xml.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

import jeng_game.MainGame;
import jeng_graphics.SpriteFrame;
import jeng_utilities.BoxInt;

public class SpriteGraph {
	
	SpriteFrame currentFrame;
	//last time a frame was grabbed in ms
	int lastFrameUpdate;
	
	RasterHandler rh;
	MainGame p;
	
	public SpriteGraph(MainGame ap)
	{
		rh = ap.myRaster;
		p = ap;
	}
	
	//need xml node of sprite
	public void setGraph(XMLElement axml)
	{
		List<SpriteFrame> visited = new LinkedList<SpriteFrame>();
		
		currentFrame = setUpFrames(axml, 1, visited);
		
		lastFrameUpdate = p.millis();
	}
	
	private SpriteFrame setUpFrames(XMLElement axml, int aid, List<SpriteFrame> visited)
	{
		Iterator<SpriteFrame> it = visited.iterator();
		//if find node than return it
		while(it.hasNext())
		{
			SpriteFrame next = it.next();
			if(next.id == aid)
				return next;
		}
		
		//no node was found so we create a new one
		//create a new xml for frame info
		XMLElement tempXML = null;		
		//find node that we want
		
		if(axml == null)
			PApplet.println("xml failed");
		
		for(int i = 0; i < axml.getChildCount(); i++)
		{
			//find element with id that we want
			if(axml.getChildAtIndex(i).getIntAttribute("id") == aid)
			{
				tempXML = axml.getChildAtIndex(i);
				break;
			}
		}
		
		//dont really need this
		if(tempXML == null)
			return null;
		
		//create new frames and set variables
		SpriteFrame tempFrame = new SpriteFrame();
		tempFrame.setVars(tempXML.getAttribute("state"), tempXML.getIntAttribute("id"), tempXML.getIntAttribute("time"));
		tempFrame.setDrawBox(
				new BoxInt(
						tempXML.getIntAttribute("x"),
						tempXML.getIntAttribute("y"),
						tempXML.getIntAttribute("w"),
						tempXML.getIntAttribute("h")
						)
				);
		//set velocity
		tempFrame.velocity = tempXML.getIntAttribute("velx");
		
		//set image
		tempFrame.myImage = rh.getImage(axml.getAttribute("file"));
		
		//BAAAAAAAAAAAAAAAD SO BAD SO BAD the pointetrs point to the same place SO BAD
		tempFrame.addHitBox(tempFrame.myDrawBox);
	
		
		//put yourself in list of visited frames
		visited.add(tempFrame);
		
		//recursively add next frame
		for(int i = 0; i < tempXML.getChildCount(); i++)
		{
			tempFrame.addFrame(
					setUpFrames(
							axml, 
							tempXML.getChildAtIndex(i).getIntAttribute("id"),
							visited),
					tempXML.getChildAtIndex(i).getStringAttribute("state"));
		}
		
		//return yourself
		return tempFrame;
	}
	
	public SpriteFrame grabFrame(String astate)
	{
		//while the time since last frame update is gr
		while(p.millis() - lastFrameUpdate > currentFrame.time)
		{
			//advance the time
			lastFrameUpdate += currentFrame.time;
			currentFrame = currentFrame.getFrame(astate);
		}
		
		return currentFrame;
	}
	
	public int getTimeSinceLastUpdate()
	{
		return p.millis()-lastFrameUpdate;
	}
}
