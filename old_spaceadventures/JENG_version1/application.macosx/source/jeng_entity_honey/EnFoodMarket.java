package jeng_entity_honey;
import java.util.LinkedList;
import java.util.List;

import processing.core.*;
import jeng_entity.*;
import jeng_game.*;
import jeng_utilities.*;

public class EnFoodMarket extends Entity {
	
	//info about square size
	int blockSize = 25;
	int gridSpace = 30;
	
	Matrix<Cell> mtx;
	
	PairFloat pos = new PairFloat(-2000,-1000);
	
	public EnFoodMarket(MainGame ap)
	{
		super(ap);
		mtx = new Matrix<Cell>(200,100);
		createEmptyCells();
		
	}
	
	public void update()
	{
		
		
		for(int j = 0; j < mtx.nLength; j++)
		{
			for(int i = 0; i < mtx.nWidth; i++ )
			{
				if((int)p.random(0,50000) == 1 && mtx.at(i,j) == null)
				{
					Cell newCell = new Cell(p,mtx,i,j);
					
					mtx.set(i, j, newCell);
					
					newCell.color.alive = true;
					newCell.color.r = (int)p.random(200,255);
					newCell.color.g = (int)p.random(200,255);
					newCell.color.b = (int)p.random(200,255);
				}
			}
		}
		
		for(Cell cl : mtx.ls)
		{
			if(cl != null)
				cl.sell();
		}
		for(Cell cl : mtx.ls)
		{
			if(cl != null)
				cl.relax();
		}
		for(Cell cl : mtx.ls)
		{
			if(cl != null)
				cl.evolve();	
		}
		for(int i = 0; i < mtx.size(); i++)
		{
			//if its dead
			if(mtx.ls.get(i) != null && mtx.ls.get(i).color.alive == false)
				mtx.ls.set(i, null);
		}

	}
	
	public void createRecursive(int x, int y)
	{
		
	}

	private void createEmptyCells()
	{
		for(int i = 0; i < mtx.nWidth * mtx.nLength; i++)
		{			
			mtx.ls.add(null);
		}
	}
	
	public void draw()
	{
		
		for(int j = 0; j < mtx.nLength; j++)
		{
			for(int i = 0; i < mtx.nWidth; i++ )
			{
				if(mtx.at(i, j) != null && mtx.at(i, j).color.alive)
				{
					p.noStroke();
					p.fill(mtx.at(i, j).color.r,
							mtx.at(i, j).color.g,
							mtx.at(i, j).color.b);
					
					p.rect(pos.x + i*gridSpace, pos.y + j*gridSpace, blockSize, blockSize);
					p.noFill();
				}
			}
		}	
		
	}
	
	
}