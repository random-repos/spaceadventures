package jeng_entity_honey;

public class DNA {
	
	public boolean alive = false;
	public int iteration = 0;

	int r = 255;
	int g = 255;
	int b = 255;
	
	public DNA()
	{
		
	}
	
	public DNA(int r_, int g_, int b_)
	{
		r = r_;
		g = g_;
		b = b_;
	}
	
	public DNA(int r_, int g_, int b_, boolean alive_)
	{
		r = r_;
		g = g_;
		b = b_;
		
		alive = alive_;
	}
	
	public DNA copy()
	{
		DNA tempDNA = new DNA(r, g, b, alive);
		tempDNA.iteration = iteration;
		return tempDNA;
	}
	

}
