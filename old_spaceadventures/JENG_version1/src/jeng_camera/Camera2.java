package jeng_camera;

import processing.core.*;
import jeng_game.*;
import jeng_utilities.PairFloat;
import jeng_utilities.Timer;
import jeng_entity.EnHit;

public class Camera2 {
	
	public PairFloat pos = new PairFloat();
	public float angle = (float)Math.PI/2;
	public float zoom = 1;
	
	Timer time;
	PAppletb p;
	
	public Camera2(PAppletb ap)
	{
		p = ap;
		time = new Timer(p);
	}
	
	public String toString() { return "Camera2"; }
	
	private void applyTransformation()
	{	
		p.scale(zoom);
		
		//translate to edge of screen
		p.translate(-pos.x , -pos.y );
		//translate to center of position
		p.translate(p.width/2/zoom, p.height/2/zoom);
		
		//rotate
		p.rotateAt((float)(angle - Math.PI/2), pos.x, pos.y);
		
		//p.rotate( angle );	
	}
	
	private void applyTransformation(float pScale)
	{	
		p.scale(zoom);
		
		//translate to edge of screen
		p.translate(-pos.x/pScale , -pos.y/pScale );
		//translate to center of position
		p.translate(p.width/2/zoom/pScale, p.height/2/zoom/pScale);
		
		//rotate
		p.rotateAt((float)(angle - Math.PI/2), pos.x/pScale, pos.y/pScale);
		
		//p.rotate( angle );	
	}
	
	public void feed()
	{	
		p.pushMatrix();
		applyTransformation();
	}
	
	public void update()
	{
		time.update();
	}
	
	public void feed(float pScale)
	{
		p.pushMatrix();
		applyTransformation(pScale);
	}
	
	public void reverseTransform()
	{
		p.popMatrix();
	}
	
	public void translate(float x, float y)
	{
		pos.x -= x;
		pos.y -= y;
	}
	
	public void move(float x, float y)
	{
		pos.x = x;
		pos.y = y;
	}
	
	public void setPosition(EnHit obj)
	{
		setPosition(obj.pos.x,obj.pos.y);
	}
	
	public void setPosition(float x, float y)
	{
		pos.x = x;
		pos.y = y;
	}
	
	
	public void follow(float x, float y)
	{
		pos.x -= (pos.x - x)*2*time.getSecPast();
		pos.y -= (pos.y - y)*2*time.getSecPast();
	}
	
	public void follow(EnHit obj)
	{
		follow(obj.pos.x, obj.pos.y);
		//move towards obj and speed proportional to distance
		/*
		pos.x -= (pos.x - obj.pos.x)*2*time.getSecPast();
		pos.y -= (pos.y - obj.pos.y)*2*time.getSecPast();
		*/
	}
	
	public void easeRot(float aangle)
	{
		angle -= (angle-aangle)*time.getSecPast();
	}
	
	public void easeZoom(float azoom)
	{
		zoom -= (zoom-azoom)*time.getSecPast();
	}
	
	public void shake(float ar)
	{
		pos.x = pos.x + p.random( -ar, ar );
		pos.y = pos.y + p.random( -ar, ar );
	}
	
	public PairFloat getMousePos(int ax, int ay)
	{
		PairFloat m = new PairFloat(ax,ay);
		
		
		//reverse rotate
		//IMPLEMENT
		
		//reverese zoom
		m.x /= zoom;
		m.y /= zoom;
		
		//reverse translate
		m.x += pos.x - p.width/2/zoom;
		m.y += pos.y - p.height/2/zoom;
		
		
		
		/*
		m.y = p.modelY(ax, ay, 0);
		m.x = p.modelX(ax, ay, 0);
		*/
		
		return m;
	}
}

