package jeng_entity_honey;

import java.util.Iterator;
import java.util.List;

import processing.core.PApplet;
import jeng_game.*;

public class Cell {
	
	MainGame p;
	Matrix<Cell> mtx;
	private List<Cell> lsFriends;
	
	public DNA color = new DNA();
	public DNA nextColor = new DNA();
	
	public int sellCount = 0;
	
	int x, y;
	
	//explosion 1
	//conway 2
	int ruleSet = 1;
	
	boolean changed;
	
	public Cell( MainGame _p, Matrix<Cell> mtx_, int x_, int y_ ) { p = _p; x = x_; y = y_; mtx = mtx_; }
	
	public void init( List<Cell> lsFriends_ )
	{
		lsFriends = lsFriends_;
	}
	
	public void relax()
	{
		if(changed == false)
		{
			nextColor = new DNA();
		}
		
		changed = false;
	}
	
	public void research()
	{
		
		
		//averaging
		int nCounter = 0;
		int rc = 0;
		int gc = 0;
		int bc = 0;
		
		for(Iterator<Cell> it = lsFriends.iterator(); it.hasNext();)
		{
			nCounter++;
			
			Cell tempCell = it.next();
			
			rc += (tempCell.color.h) - (int)p.random(0,4) + 1;
			gc += (tempCell.color.s) - (int)p.random(0,4) + 1;
			bc += (tempCell.color.b) - (int)p.random(0,4) + 1;
		}
		
		rc /= nCounter;
		gc /= nCounter;
		bc /= nCounter;
		
		nextColor = new DNA(rc, gc, bc);
		
	}
	
	public void sell()
	{
		
		//if we have honey
		if(color.alive)
		{
			explosions();
		}	
		
		/*
		for(Iterator<Cell> it = lsFriends.iterator(); it.hasNext();)
		{
			Cell tempCell = it.next();
			if( p.random(0,color.iteration) < 1)
			{
				//has changed
				tempCell.changed = true;
				//copy color
				tempCell.nextColor = color.copy();
				//increase the iteration
				tempCell.nextColor.iteration = tempCell.nextColor.iteration+1;
			}
		}
		*/
	}

	public void evolve()
	{
		
		boolean live = false;
		
		//if currently alive
		if(color.alive)
		{
			if(sellCount != 2 || sellCount != 3 )
				live = false;
		}
		else
		{
			if(sellCount == 3 )
				live = true;
		}
		
		color = nextColor.copy();
		
		//if conway
		if(ruleSet == 2)
		{
			color.alive = live;
		}
	
		
		sellCount = 0;
		
		
	}	
	
	public void kill(boolean alive_)
	{
		color.alive = false;
	}
	public void live()
	{
		color.alive = true;
	}
	
	private void explosions()
	{
		for(int j = -1; j <= 1; ++j)
			for(int i = -1; i <= 1; ++i)
			{
				if(i == 0 && j == 0)
					continue;
				
				//grab adjacent cell
				Cell c = mtx.at(i + x, j + y);
				
				//random based on iteration
				if( (ruleSet == 1 && p.random(0,color.iteration) < 1) || ruleSet == 2)
				{
					if(c != null)
					{	
						//has changed
						c.changed = true;
						//copy color
						c.nextColor = color.copy();
						//increase the iteration
						c.nextColor.iteration = c.nextColor.iteration+1;
						c.sellCount++;
					}
					else
					{
						//create  new cell
						Cell newCell = new Cell(p,mtx,i+x,j+y);
						newCell.changed = true;
						newCell.nextColor = color.copy();
						newCell.nextColor.iteration++;
						newCell.sellCount++;
						mtx.set(i+x, j+y, newCell);
						
					}
				}
			}
	}
}
