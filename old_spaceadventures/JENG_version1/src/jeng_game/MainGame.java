package jeng_game;

import processing.core.*;

import processing.opengl.*;
import jeng_camera.*;
import jeng_container.ColContainer;
import jeng_container.Message;
import jeng_entity.*;
import jeng_entity_planet.*;
import jeng_entity_sprite.*;
import jeng_utilities.*;
import jeng_graphics.*;
import jeng_entity_honey.*;

import jmcvideo.*;

public class MainGame extends PAppletb
{
	
	//JMCMovie myMovie;
	
	public RasterHandler myRaster = new RasterHandler(this);
	public Camera2 myCam;
	public ColContainer myCont;
	
	float zoomDenom = 500;
	float bgColor = 200;

	Entity mySprite;
	EnHit followEn;
	
	//stupid stuff
	
	public KeyStruct myKeys = new KeyStruct();
	
	public static void main(String args[])	{ PApplet.main(new String[] { "--present", "jeng_game.MainGame" } );	}
	
	public void setup()
	{
		
	
		 //set up a screen
		 //size(screen.width, screen.height, OPENGL);		
		 size(screen.width, screen.height);
		 //frameRate(30);
		 //ortho();
		
		 
		 //myMovie = new JMCMovie(this, "angrycat.flv");
		 
		 //this needs to be put into levelManager class
		 myCam = new Camera2(this);
		 myCont = new ColContainer(this);

		
		 //myCont.addEntity(new EnFoodMarket(this));
		 
		 
		 
		 //border
		 myCont.addEntity(new EnBorder(this));
		 ((EnHit)myCont.getLast()).setPosition(-1500, 550);
		 
		 //floor
		 myCont.addEntity(new EnRectangle(this));
		 ((EnHit)myCont.getLast()).setPosition(-2000, 575);
		 
		
		
		 
		 
		 /*
		 myCont.addEntity(new EnZafu(this));
		 ((EnZafu)myCont.getLast()).setPosition(200, 500);
		//painting
		 myCont.addEntity(new EnPainting(this));
		 ((EnHit)myCont.getLast()).setPosition(0, 350);
		 myCont.addEntity(new EnPainting(this));
		 ((EnHit)myCont.getLast()).setPosition(200, 350);
		*/
		 
		 
		
		 //planets!
		 //1
		 myCont.addEntity(new EnPlanet(this));
		 ((EnPlanet)myCont.getLast()).setPosition(1900, -300);
		 ((EnPlanet)myCont.getLast()).mass = 300;
		 ((EnPlanet)myCont.getLast()).radius = 300;
		 
		//planets!
		 //2
		 myCont.addEntity(new EnPlanet(this));
		 ((EnPlanet)myCont.getLast()).setPosition(3100, -456);
		 ((EnPlanet)myCont.getLast()).mass = 300;
		 ((EnPlanet)myCont.getLast()).radius = 400;
		 
		//planets!
		//3
		 myCont.addEntity(new EnPlanet(this));
		 ((EnPlanet)myCont.getLast()).setPosition(3500, -1200);
		 ((EnPlanet)myCont.getLast()).mass = 250;
		 ((EnPlanet)myCont.getLast()).radius = 300;
		 
		//planets!
		//4
		 myCont.addEntity(new EnPlanet(this));
		 ((EnPlanet)myCont.getLast()).setPosition(4500, -500);
		 ((EnPlanet)myCont.getLast()).mass = 400;
		 ((EnPlanet)myCont.getLast()).radius = 500;
		 
		 
		 //EnSpriteMy
		 mySprite = new EnSpriteGroundMan(this);
		 ((EnHit)mySprite).setPosition(-400, 440);
		 myCont.addEntity(mySprite);
		 myCam.setPosition(((EnHit)mySprite).pos.x, ((EnHit)mySprite).pos.y-200);
		 
		 
		 
		 myCont.addEntity(new EnZafu(this));
		 ((EnZafu)myCont.getLast()).setPosition(-200, 520);
		 
		 
		 
		 //television
		 myCont.addEntity(new EnSpriteStatic(this,"tvback.xml", (float).8));
		 ((EnHit)myCont.getLast()).setPosition(-100, 200);
		
		 
		 //gridfade
		 //myCont.addEntity(new EnGridFade(this));
		 
		 //myCont.addEntity(new EnPlanetEditor(this));
		 
		 //myCont.addEntity(new EnTvStatic(this));
	}
	
	int last = 0;
	public void draw()
	{
		
		
		//wipe screen
		background(bgColor);
		
		//fill(255);
		//rect(0,600,2000,1000);
		
		handleCamera();
		
		script_01();
		
		//update 
		myCont.update();
		//message
		myCont.handleMessage();
		//draw
		myCont.draw();
		
		myCam.reverseTransform();
		myCam.update();
		
		//println(millis()-last);
		last = millis();
	}
	
	
	
	//put to handle input function in MainFunctions
	public void keyPressed()
	{
		if(keyCode == TAB)
			noLoop();
		
		if(keyCode == UP)
			myKeys.up = true;
		if(keyCode == DOWN)
			myKeys.down = true;
		if(keyCode == LEFT)
			myKeys.left = true;
		if(keyCode == RIGHT)
			myKeys.right = true;
		if(keyCode == SHIFT)
			myKeys.rtn = true;
		
		if(keyCode == ESC)
			exit();
	}
	
	public void keyReleased()
	{
		if(keyCode == TAB)
			loop();
		
		if(keyCode == UP)
			myKeys.up = false;
		if(keyCode == DOWN)
			myKeys.down = false;
		if(keyCode == LEFT)
			myKeys.left = false;
		if(keyCode == RIGHT)
			myKeys.right = false;
		if(keyCode == SHIFT)
			myKeys.rtn = false;
	}
	
	public void mousePressed()
	{
		if(((EnPlanetEditor)(myCont.getEntity("EnPlanetEditor"))).mode)
		{
			EnPlanet tempEntity = new EnPlanet(this);
			//tempEntity.setPosition(myCam.pos.s.x - mouseX, myCam.pos.s.y - mouseY);
			tempEntity.setPosition( myCam.getMousePos(mouseX, mouseY).x,  myCam.getMousePos(mouseX, mouseY).y);
			tempEntity.mass = ((EnPlanetEditor)(myCont.getEntity("EnPlanetEditor"))).massVar;
			tempEntity.radius = ((EnPlanetEditor)(myCont.getEntity("EnPlanetEditor"))).sizeVar;
			myCont.addEntity(tempEntity);
	
			
			Message tmpMsg = new Message(null);
			tmpMsg.toString = "EnPlanetEditor";
			myCont.sendMessage(tmpMsg);
		}
	}
	
	private void swapMan()
	{
		if(mySprite.toString() == "EnSpriteGroundMan")
		{
			int tempX = (int)((EnHit)mySprite).pos.x;
			int tempY = (int)((EnHit)mySprite).pos.y;
			myCont.removeEntity(myCont.getEntity("EnSpriteGroundMan"));
			mySprite = new EnSpriteMoonMan(this);
			((EnHit)mySprite).setPosition(tempX, tempY);
			//((EnSpriteMoonMan)mySprite).
			myCont.addEntity(mySprite);
		}
	}
	
	private void handleCamera()
	{
		//set camera
		myCam.follow(((EnHit)mySprite).pos.x,((EnHit)mySprite).pos.y-100);
		
		
		if(mySprite instanceof EnSpriteMoonMan)
			zoomDenom = 800;
		else
			zoomDenom = 200;
		
		//PairFloat tempPos = new PairFloat( ((EnHit)mySprite).pos.x, ((EnHit)mySprite).pos.y - 100);
		//myCam.easeZoom(((float)(1-LineFcn.getLength(myCam.pos, tempPos)/zoomDenom)));
		//myCam.zoom = (float).2;
		
		if(mySprite  instanceof EnSpriteMoonMan)
			myCam.easeRot((float)((EnSpriteMoonMan)mySprite).rotAngle);
		
		//myCam.shake(3);
		
		myCam.feed();
	}
	
	private void script_01()
	{
		
		if( ((EnHit)mySprite).pos.x > 500 && ((EnHit)mySprite).pos.x < 1500)
			bgColor = lerp( 200, 50, (((EnHit)mySprite).pos.x-500)/1000 );
		if( ((EnHit)mySprite).pos.x > 1500 && ((EnHit)mySprite).pos.x < 4000)
			bgColor = lerp( 50, 0, (((EnHit)mySprite).pos.x-1500)/2500 );
		
		if(((EnHit)mySprite).pos.x > 1000 )
			swapMan();
		
		if(((EnHit)mySprite).pos.x > 5300 )
		{
			((EnSpriteMoonMan)myCont.getEntity("EnSpriteMoonMan")).planetList.clear();
			((EnGridFade)(myCont.getEntity("EnGridFade"))).fadeOut();
		}
	}
}

