package jeng_entity_planet;

import processing.core.*;
import jeng_container.Message;
import jeng_entity.Entity;
import jeng_game.MainGame;
import jeng_utilities.HexColor;
import jeng_utilities.PairFloat;
import jeng_utilities.Timer;

//enPlanet has its own special hit detection that is handled in EnSpriteMy class directly
public class EnPlanet extends Entity {
	//timer
	Timer time;
	//position
	public PairFloat pos = new PairFloat(900,500);
	public float radius = 300;
	public float mass = 400;
	
	HexColor color = null;
	
	PShape myShape;
	
	public EnPlanet(MainGame ap)
	{
		super(ap);
		
		time = new Timer(ap);
		
		//load random svg file from random svg file list
		myShape = p.loadShape("planet_02.svg");
		
		//color
		color = new HexColor();
		color.setRandom_light(p);
		
		//send msg to EnSpriteMy
		//old useless stupid etc
		Message tempMsg = new Message(this);
		tempMsg.toString = "EnSpriteMy";
		msgStack.push(tempMsg);
		
		//send msg to EnSpriteMy
		tempMsg = new Message(this);
		tempMsg.toString = "EnSpriteMoonMan";
		msgStack.push(tempMsg);
		

	}
	
	public String toString() {	return "EnPlanet"; }
	
	//i.e. teleport fcn
	public void setPosition( float x, float y )
	{
		pos.x = x;
		pos.y = y;
	}
	
	public void sendMessage(Message amsg)
	{
		if(amsg.from instanceof EnSpriteMoonMan)
		{
			Message tempMsg = new Message(this);
			tempMsg.toString = "EnSpriteMoonMan";
			msgStack.push(tempMsg);
		}
	}
	
	public void draw()
	{
		
		
		
		
		
		p.pushMatrix();
		
		//svg mode
		p.shapeMode(p.CENTER);
		p.shape(myShape, pos.x, pos.y, radius*2, radius*2);
		
		
		/*
		p.translate(pos.x, pos.y, 0);
		p.rotateX(p.random(50));
		p.rotateZ(p.random(50));
		p.rotateY(p.random(50));
		
		p.noStroke();
		p.lights();
		
		p.fill(color.r, color.g, color.b);
		p.sphereDetail(30);
		p.sphere(radius);
		
		p.noLights();
		*/
		
		//solid stuff
		/*
		//p.tint(0,255);
		//p.fill(230);
		p.fill(color.r, color.g, color.b);
		//p.noFill();
		
		p.stroke(2);		
		p.ellipse(pos.x,pos.y,radius*2,radius*2);
		//p.ellipse(pos.x, pos.y, 5, 5);
		p.noStroke();
		p.noFill();
		//p.noTint();
		*/
		
		
		p.popMatrix();
		
		
	}
	
	public void update()
	{
		//spin, animate, do some fancy shit
		
		
		time.update();
	}
	
	
}
