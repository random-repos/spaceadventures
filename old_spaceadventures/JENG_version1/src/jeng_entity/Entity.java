package jeng_entity;

import jeng_container.Message;
import jeng_game.MainGame;
import java.util.Stack;


public class Entity extends ObjectEn
{
	//parent
	protected MainGame p;
	//message stack
	protected Stack<Message> msgStack = new Stack<Message>();
	//unique identifier (currently unused)
	public int id = 0;
	
	//constructor
	public Entity(MainGame ap){	p = ap; }
	
	//IDENT accesors and modifiers
	public String toString() { return "Entity"; };
	public void setId(int aid) { id = aid; };
	public int getId() { return id; };
	
	//UPDATE AND DRAW METHODS
	public void update(){}
	public void draw(){}
	
	//MESSAGE methods
	//send message to this entity
	public void sendMessage(Message amsg){}
	//checks if there is message
	public boolean isMsg(){	return !msgStack.isEmpty();	}
	//you probably wont override this ever
	//gets a message from this entity
	public Message getMessage()
	{
		if(msgStack.isEmpty())
			return null;
		else
			return msgStack.pop();
	}	
}
