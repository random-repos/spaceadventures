package jeng_entity;

import jeng_utilities.Timer;

import jeng_game.MainGame;

public class EnGridFade extends Entity{
	
	int width = 32;
	int height = 32;
	
	float sizePercent = (float)0;
	
	//0.percent per sec
	float fadeRate = (float).5;
	
	boolean fading = false;
	
	Timer time;
	
	public EnGridFade(MainGame ap)
	{
		super(ap);
		time = new Timer(ap);
	}
	
	public String toString() { return "EnGridFade"; } 
	
	public void update()
	{
		//if we are going to fade out
		if(fading)
		{
			sizePercent += time.getSecPast()*fadeRate;
			if(sizePercent > 1)
				sizePercent = 1;
		}
		else
		{
			sizePercent -= time.getSecPast()*fadeRate;
			if(sizePercent < 0)
				sizePercent = 0;
		}
		
		time.update();
	}
	
	public void fadeIn()
	{
		fading = false;
	}
	
	public void fadeOut()
	{
		fading = true;
	}
	
	public void draw()
	{
		p.myCam.reverseTransform();
		
		for(int i = 0; i * width < p.screen.width; i++)
		{
			for(int j = 0; j * height < p.screen.height; j++)
			{
				
				p.tint(0, sizePercent*255);
				p.fill(0);
				p.ellipse(i*width + width/2, j*height + height/2, (float)width*sizePercent*3/2, (float)height*sizePercent*3/2);
				p.noTint();
				p.noFill();
			}
		}
		p.myCam.feed();
	}

}
