package jeng_entity;

import jeng_game.MainGame;
import processing.core.*;

public class EnSVGStatic extends EnHit {

	PShape myShape;
	
	public EnSVGStatic(MainGame ap, String filename)
	{
		
		super(ap);
		myShape = p.loadShape(filename);

	}
	
	public void draw()
	{
		p.shape(myShape,pos.x, pos.y, myShape.width, myShape.height);
	}
	
	//get hit box
	//should return PShape hitbox
}
