package jeng_entity;

import jeng_game.MainGame;
import jeng_utilities.Rectangle;

public class EnRectangle extends EnHit {

	public int w = 10000;
	public int h = 2000;
	
	boolean show = true;
	
	public EnRectangle(MainGame ap)
	{
		super(ap);	
	}
	
	public String toString() { return "EnRectangle"; }
	
	public void draw()
	{
		//just in case
		p.noStroke();
		
		//for opengl
		p.fill(190);
		p.rect(pos.x, pos.y, pos.x+w, pos.y+h);
		p.noFill();
	}


}
