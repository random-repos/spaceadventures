package jeng_container;
import processing.core.*;
import java.util.List;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Iterator;

import jeng_entity.Entity;
import jeng_game.MainGame;

//make this extend entity?

public class Container extends Entity {

	//parent entity
	Entity en = null;
	
	//list of objects we have
	List<Entity> enList = new LinkedList<Entity>();
	//our message stack
	//Stack<Message> msgStack = new Stack<Message>();

	//
	public Container(Entity aen, MainGame ap)
	{
		super(ap);
		en = aen;
	}
	public Container(MainGame ap)
	{
		super(ap);
	}
	
	//adds an object
	public void addEntity(Entity aentity)
	{
		enList.add(aentity);
	}
	
	public Entity getLast()
	{
		return enList.get(enList.size()-1);
	}
	
	public Entity getEntity(String name)
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
		{
			Entity temp = it.next();
			if(temp.toString() == name)
				return temp;
		}
		return null;
	}
	
	public void removeEntity(Entity aen)
	{
		enList.remove(aen);
	}
	
	
	
	//draws everything
	public void draw()
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
			it.next().draw();
	}
	
	//updates everything
	public void update()
	{
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
			it.next().update();
	}
	
	//handles all msgs
	public void handleMessage()
	{
		
		//THIS HANDLES MESSAGES OF ALL CHILD OBJECTS
		//MAYBE MAKE THIS FUNCTION SEPERATE FROM getMessage
		
		//check for new msgs using get msg fcn
		for(Iterator<Entity> it = enList.iterator(); it.hasNext();)
		{
			
			Entity temp = it.next();
			//while there is msgs
			while(temp.isMsg())
			{
				//get a msg
				transferMessage(temp.getMessage());
				
			}
		}		
	}
	
	private void transferMessage( Message tempMsg )
	{
		boolean isSent = false;
		for(Iterator<Entity> it2 = enList.iterator(); it2.hasNext();)
		{
			Entity temp2 = it2.next();
			if(tempMsg.toString == temp2.toString())
			{
				if(tempMsg.toId == -1)
				{
					isSent = true;
					//send the msg
					temp2.sendMessage(tempMsg);
				}
				else if(tempMsg.toId == temp2.id)
				{
					isSent = true;
					//send the msg
					temp2.sendMessage(tempMsg);
					break;
				}
			}
		}
		
		//if nothing was sent
		//put it on our own message stack
		if(!isSent && tempMsg != null)
			msgStack.push(tempMsg);
	
		
	}
	
	public void sendMessage(Message amsg)
	{
		transferMessage(amsg);
	}
	
	public void deleteAll()
	{
		enList.clear();
	}
	
}
