package jeng_camera;

import processing.core.*;
import jeng_utilities.LineFloat;
import jeng_utilities.PairFloat;
import jeng_utilities.Timer;
import jeng_entity.EnHit;

public class Camera {
	int baseWidth; //the zoom = 0 width
	public LineFloat pos = new LineFloat();
	//LineFloat oldPos = new LineFloat();
	
	float screenRatio;
	Timer time;
	PApplet p;
	
	public Camera(PApplet ap)
	{
		p = ap;
		time = new Timer(p);
		baseWidth = p.width;
		screenRatio = p.width/p.height;
	}
	
	public String toString() { return "Camera"; };
	
	protected void finalize()
	{
	}
	
	private float getRotation(LineFloat apos)
	{
		return (float)Math.atan2((double)(apos.s.y-apos.e.y),(double)(apos.s.x-apos.e.x));
	}
	
	private float getDistance(LineFloat apos)
	{
		double tempNumber = ((apos.s.x-apos.e.x)*(apos.s.x-apos.e.x) - (apos.s.y-apos.e.y)*(apos.s.y-apos.e.y)); 
		return (float)Math.sqrt( tempNumber );
	}
	
	private float getZoom()
	{
		return getDistance(pos)/baseWidth; 
	}
	
	private void applyTransformation()
	{
		
		p.translate(pos.s.x, pos.s.y);
		p.rotate( getRotation(pos) );
		//p.scale(getZoom());
		//p.scale((float).5);

	}
	
	public void feed()
	{	
		applyTransformation();
		time.update();
	}
	
	
	public void translate(float x, float y)
	{
		pos.translate(-x, -y);
	}
	
	public void move(float x, float y)
	{
		pos.move( -x, -y );
	}
	
	public void zoom(float percent)
	{
		//extend by percent
	}
	
	//rotates rolative to the CENTER
	public void rotate(float rad)
	{
		rotate(0,0,rad);
	}
	
	//rotates the CENTER relative to x,y in absolute coordinates
	//UNFINISHED
	public void rotate(float x, float y, float rad)
	{
		float halfWidth = getDistance(pos)/2;
		float halfHeight = (getDistance(pos)/screenRatio) / 2;
		//PairFloat(x+)
		//calculate center
	}
	
	public PairFloat getMousePos(float ax, float ay)
	{
		PairFloat tempPf = new PairFloat();
		
		return tempPf;
	}
	
	public void follow(EnHit obj)
	{
		//move at velocity towards object proportional to distance
	}
	
	
}
