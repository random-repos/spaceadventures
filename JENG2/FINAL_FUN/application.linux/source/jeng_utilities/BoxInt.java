package jeng_utilities;

public class BoxInt 
{
	public String name;
	
	public int x = 0;
	public int y = 0;
	public int w = 0;
	public int h = 0;
	
	public BoxInt(){};
	public BoxInt(int ax, int ay, int aw, int ah)
	{
		x = ax;
		y = ay;
		w = aw;
		h = ah;
	}
}
