package jeng_entity;

import jeng_container.Message;
import jeng_entity_planet.EnSpriteMoonMan;
import jeng_game.*;
import jeng_utilities.Timer;
import ddf.minim.*;
import ddf.minim.signals.*;

public class EnNoise extends Entity {
	
	Minim myMin;
	AudioOutput out;
	SineWave sine;

	Timer time;
	
	public EnNoise(MainGame ap)
	{
		super(ap);
		myMin = new Minim(ap);
		out = myMin.getLineOut(Minim.STEREO);
		// create a sine wave Oscillator, set to 440 Hz, at 0.5 amplitude, sample rate from line out
		sine = new SineWave(440, (float)0.5, out.sampleRate());
		// set the portamento speed on the oscillator to 200 milliseconds
		sine.portamento(200);
		// add the oscillator to the line out
		out.addSignal(sine);
		  
		time = new Timer(ap);
	}
	
	public void finalize()
	{
		out.close();
		myMin.stop();

	}
	
	public String toString() { return "EnNoise"; }
	
	public void update()
	{
		//decrease volume steadily
		out.setVolume(out.getVolume()/(1+5*time.getSecPast()));
		
		sine.setAmp(sine.amplitude()/(1+5*time.getSecPast()));
		time.update();
		
	}
	
	public void sendMessage(Message amsg)
	{
		if(amsg.from instanceof EnSpriteMoonMan)
		{
			if( ((EnSpriteMoonMan)amsg.from).projPos.time > 100 )
				if(sine.amplitude() < 50 )
					sine.setAmp((float)(sine.amplitude()+1));
				
			//sine.setFreq( 5000/((EnSpriteMoonMan)amsg.from).projPos.time);
			if(p.random(10) < 1)
				sine.setFreq( p.random(200,1000) );
		}
	}
}

class SineWave extends Oscillator
{
  public SineWave(float frequency, float amplitude, float sampleRate)
  {
    super(frequency, amplitude, sampleRate);
  }
 
  protected float value(float step)
  {
    return (float)Math.sin(frequency()*TWO_PI*step);
  }
}
