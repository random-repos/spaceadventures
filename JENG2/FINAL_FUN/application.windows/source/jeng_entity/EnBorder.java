package jeng_entity;

import jeng_game.MainGame;
import jeng_utilities.PairFloat;
import jeng_utilities.Rectangle;

public class EnBorder extends EnHit {
	
	public int w = 4000;
	public int h = 0;
	
	boolean show = false;
	
	public EnBorder(MainGame ap)
	{
		super(ap);	
	}
	
	public String toString() { return "EnBorder"; }
	
	public Rectangle getHitRect()
	{
		
		hitRect.x = pos.x;
		hitRect.y = pos.y;
		hitRect.w = w;
		hitRect.h = h;
		return hitRect;
	}
	
	public void draw()
	{
		if(show)
		{
			p.stroke(255);
			p.line(pos.x, pos.y, pos.x+w, pos.y+h);
			p.noStroke();
		}
	}

}
