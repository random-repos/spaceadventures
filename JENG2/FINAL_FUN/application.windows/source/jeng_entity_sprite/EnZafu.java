package jeng_entity_sprite;

import jeng_container.Message;
import jeng_entity.EnSprite;
import jeng_game.MainGame;
import jeng_utilities.Rectangle;

public class EnZafu extends EnSprite 
{
	//we are in contact with EnSpriteMy
	boolean isHit = false;
	boolean isMsg = false;
	
	public EnZafu(MainGame ap)
	{
		//need to fix this so it grabs child with attribute name = UP DOWN LEFT or RIGHT
		//currently just grabs the first thing
		super(ap, "zafu2.xml", "sprite");
		myState = "DEFAULT";
	}
	
	public String toString() { return "EnZafu"; }
	
	public void update()
	{
		//if we did not get a message last cycle
		if(!isMsg)
			myState = "DEFAULT";
		
		//assume there is no message
		isMsg = false;
	
	}
	
	public void sendMessage(Message amsg)
	{
		//we get a message saying it has been hit, set myState to POOF
		//set it back to not POOF
		if(amsg.from instanceof EnSpriteGroundMan)
			myState = "POOF";
		
		//we did get a message
		isMsg = true;
	}
	
	public void draw()
	{
		//p.println(myState);
		super.draw();
	}
	
	public Rectangle getHitRect()
	{
		hitRect.x = pos.x+70;
		hitRect.y = pos.y;
		hitRect.w = -40;
		hitRect.h = 100;
		return hitRect;
	}
}
