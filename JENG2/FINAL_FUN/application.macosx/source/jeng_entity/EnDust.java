package jeng_entity;

import java.util.Iterator;

import jeng_container.Container;
import jeng_container.Message;
import jeng_entity_sprite.*;
import jeng_entity_planet.*;
import jeng_game.MainGame;
import jeng_utilities.Timer;

class Dust extends Entity
{			
	
	
	//range of dusting force
	float dustForceMin = 0;
	float dustForceMax = 100;
	
	//particles
	int particleCount = 300;
	
	//when did we start dusting
	int startTime;
	
	//parent timer;
	Timer t;
	
	public Dust(MainGame ap, Timer at)
	{
		super(ap);
		startTime = at.getTime();
		t = at;
	}
	
	public void draw()
	{
		
	}
	
	public void update()
	{
		
		
	}
}

class DustSprite extends EnSprite
{
	//time dust lasts in ms
	int dustTime = 400;
	int startTime = 0;
	
	public DustSprite(MainGame ap)
	{
		super(ap, "dust.xml", "sprite");
		startTime = time.getTime();
	}

	
	public void draw()
	{
		//fading
		//p.tint( 255 * ( time.getTime() - startTime)/dustTime );
		//typical draw routine
		super.draw();
		//p.noTint();
	
	}
	
	public void update()
	{
		if(time.getTime() - startTime > dustTime)
			msgStack.push(new Message(this));
	}
	
}

public class EnDust extends Entity
{	
	Container myCont;
	Timer time;
	
	public EnDust (MainGame ap)
	{
		super(ap);
		time = new Timer(ap);
		myCont = new Container(this, ap);
	}
	
	public String toString() { return "EnDust"; }
	
	public void update()
	{
		//see if we ned to delete any dust
		while(myCont.isMsg())
		{
			Entity tempDust = myCont.getMessage().from;
			if(tempDust instanceof DustSprite)
			{
				myCont.removeEntity(tempDust);
			}
		}
		
		myCont.update();
	}
	
	public void draw()
	{
		myCont.draw();
	}
	
	private void trigger()
	{
		myCont.addEntity(new DustSprite(p));
		((DustSprite)myCont.getLast()).setPosition(0, 0);
	}
	
	public void sendMessage(Message amsg)
	{
		//check if message is coming from right place
		if(amsg.from instanceof EnSpriteGroundMan || amsg.from instanceof EnSpriteMoonMan)
		{
			trigger();
		}
	}
	
}
