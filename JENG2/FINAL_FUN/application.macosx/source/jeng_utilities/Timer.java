package jeng_utilities;
import processing.core.*;

public class Timer {
	PApplet p;
	
	int start;
	int lastUpdate;
	
	public Timer(PApplet ap)
	{
		p = ap;
		restart();
	}
	
	public float getTimePast()
	{
		return (p.millis() - lastUpdate)*(float)1.3;
	}
	
	public float getSecPast()
	{
		return getTimePast()/1000;
	}
	
	public void update()
	{
		lastUpdate = p.millis();
	}
	
	public float updateAndPrintFPS(int mod)
	{
		float fps = printFPS(mod);
		update();
		return fps;
	}
	
	
	public int getTimeSinceStart()
	{
		return p.millis()-start;
	}
	
	public int getTime()
	{
		return p.millis();
	}
	
	//prints fps once ever mod seconds
	//this fcn must be called right before update;
	//put 0 for no printing
	public float printFPS(int mod)
	{
		float fps = 1/getSecPast();
		
		if(p.millis()%(mod*1000) == 1)
			PApplet.println(fps);
		
		return fps;
	}
	
	public void restart()
	{
		start = p.millis();
		lastUpdate = start;
	}
	
	
	
}
