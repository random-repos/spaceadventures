package jeng_utilities;
import processing.xml.*;
public class XMLHelper {
	static XMLElement getChildWithAttribute(XMLElement aXML, String name, String attribute, String value)
	{
		for(int i = 0; i < aXML.getChildCount(); i++)
		{
			XMLElement tempElt = aXML.getChildAtIndex(i);
			if( tempElt.getName() == name )
				if(tempElt.getStringAttribute(attribute) == value)
					return tempElt;
		}
		return null;
	}
}
