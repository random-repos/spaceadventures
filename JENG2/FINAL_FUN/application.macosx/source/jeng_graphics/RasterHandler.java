package jeng_graphics;
import processing.core.*;
import java.util.Map;
import java.util.HashMap;

public class RasterHandler 
{
	
	PApplet p;
	
	RasterHandler myRef = null;
	
	//our map of images
	static Map<String, PImage> imageMap = new HashMap<String, PImage>();
	String dir="";
	
	public RasterHandler( PApplet ap, String adir )
	{
		p = ap;
		dir = adir;
		dir.concat("/");
	}
	public RasterHandler( PApplet ap )
	{
		p = ap;
	}
	
	public PImage getImage( String filename )
	{
		
		
		//if(true)
		//	return p.loadImage(filename);
		
		//if image is in the map, return it
		if(imageMap.containsKey(filename))
		{
			return imageMap.get(filename);
		}
	
		//otherwise load the image
		try
		{
			imageMap.put(filename, p.loadImage(filename) );
		}
		catch ( Exception e)
		{
			e.printStackTrace();
		}
		
		return imageMap.get(filename);
		
	}
	
}
