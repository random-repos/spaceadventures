package jeng_entity_planet;

import controlP5.*;
import jeng_container.Container;
import jeng_container.Message;
import jeng_entity.*;
import jeng_game.*;

import processing.core.PApplet;
import processing.xml.*;

public class EnPlanetEditor extends Entity {
	
	ControlP5 cp5;
	
	public int sizeVar = 200;
	public int massVar = 200;
	
	Slider sizeSlide;
	Slider massSlide;
	
	Container myCont;
	XMLElement myXML;
	
	public boolean mode;
	boolean rtnFlag = false;
	
	public EnPlanetEditor(MainGame ap)
	{
		super(ap);
		cp5 = new ControlP5(ap);
		
		myXML = new XMLElement();
		
		myCont = new Container(this, ap);
		myCont.addEntity(new EnPlanet(ap));
		
		sizeSlide = cp5.addSlider("setSize", 100, 600, sizeVar, 50, 50, 200, 50 );
		massSlide = cp5.addSlider("setMass", 100, 600, massVar, 50, 110, 200, 50 );
	}
	 
	public String toString() { return "EnPlanetEditor"; }
	
	public void update()
	{
		sizeVar = (int)sizeSlide.value();
		massVar = (int)massSlide.value();
		
		if(p.myKeys.rtn == true)
		{
			if(rtnFlag == false)
			{
				rtnFlag = true;
				mode = !mode;
			}
		}
		else
			rtnFlag = false;
		
		((EnPlanet)myCont.getLast()).setPosition(p.myCam.getMousePos(p.mouseX, p.mouseY).x,((EnPlanet)myCont.getLast()).pos.y = p.myCam.getMousePos(p.mouseX, p.mouseY).y);	
		((EnPlanet)myCont.getLast()).radius = sizeVar;
		((EnPlanet)myCont.getLast()).mass = massVar;
		
		if(myCont.isMsg())
		{
			msgStack.push(myCont.getMessage());
		}
	}
	
	public void draw()
	{
		if(mode)
			myCont.draw();
		
		p.myCam.reverseTransform();
		cp5.draw();
		p.myCam.feed();
	}
	
	public void sendMessage(Message amsg)
	{
		//if null, i.e. came from game
		if(amsg.from == null)
		{
			if(mode)
			{
				//XMLElement;
				PApplet.print( "xpos: " );
				PApplet.print( ((EnPlanet)myCont.getLast()).pos.x );
				PApplet.print( " ypos: " );
				PApplet.print( ((EnPlanet)myCont.getLast()).pos.y );
				PApplet.print( " radius: " );
				PApplet.print( ((EnPlanet)myCont.getLast()).radius );
				PApplet.print( " mass: " );
				PApplet.println ( ((EnPlanet)myCont.getLast()).mass );
			}
		}
	}
	
}
