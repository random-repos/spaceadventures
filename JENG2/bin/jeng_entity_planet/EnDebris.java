package jeng_entity_planet;

import java.util.List;
import java.util.Vector;

import jeng_container.ColContainer;
import jeng_container.Container;
import jeng_entity.EnHit;
import jeng_entity.Entity;
import jeng_game.MainGame;
import jeng_utilities.PairInt;

public class EnDebris extends EnHit {
	
	
	int layers = 4;
	double depth[] = {.4,.5,.55,.6};
	int density[] = {2,4,4,6};
	double spacing [] = {150,150,150,150};
	List<ColContainer> contList = new Vector<ColContainer>();
	
	public EnDebris(MainGame ap)
	{
		super(ap);
		
		for(int i  = 0; i < layers; i++)
		{
			ColContainer tempCont = new ColContainer(ap);
			tempCont.colScript = 2;
			for(int j = 0; j < density[i]; j++ )
			{
				
				
				int spaceSize = (int)spacing[i]*density[i];
				int counter = 0;
				while(tempCont.enList.size() < density[i] || counter < 20)
				{
					counter++;
					
					boolean success = true;
					float velX = -p.random(10,100);
					float velY = p.random(10,100)-50;
					float rotVel = p.random(0,100);
					
					int posX = (int)p.random(spaceSize);
					int posY = (int)p.random(spaceSize);
					
					//make a new entity
					EnSpriteFloatRotate tempEn = new EnSpriteFloatRotate(ap, "tvfront.xml", rotVel,velX,velY,posX,posY);
					
					//check for hit
					for(Entity en : tempCont.enList)
					{
						if(en instanceof EnHit)
						{
							if(tempCont.checkCollision(tempEn.getHitRect(), ((EnHit)en).getHitRect()))
							{
								//if there was a hit, we say it was a failure
								success = false;
								break;
							}
						}
					}
					
					if(success)
						tempCont.addEntity(tempEn);
				}
			}
			
			
			contList.add(tempCont);
		}
	}
	 
	public String toString() { return "EnDebris"; }
	
	public void update()
	{
		for(int i = 0; i < layers; i++)
			contList.get(i).update();
	}
	public void draw()
	{
		//for each layer
		for(int i = 0; i < layers; i++)
		{
			p.myCam.reverseTransform();
			p.myCam.feed((float)depth[i]);
			
			p.pushMatrix();
			p.translate(pos.x, pos.y);
			p.scale((float)(depth[i]));
			contList.get(i).draw();
			p.popMatrix();
			
			p.myCam.reverseTransform();
			p.myCam.feed();
		}
	}

}
