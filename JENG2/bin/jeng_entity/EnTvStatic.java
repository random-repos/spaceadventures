package jeng_entity;

import processing.core.PImage;
import processing.xml.XMLElement;
import jeng_game.MainGame;
import jeng_graphics.SpriteFrame;
import jeng_graphics.SpriteGraph;
import jeng_utilities.PairFloat;
import jeng_utilities.Rectangle;
import jeng_utilities.Timer;

public class EnTvStatic extends Entity{
	PImage myStatic;
	public EnTvStatic(MainGame ap)
	{
		super(ap);
		myStatic = new PImage(1000,1000);
	}
	
	public void update()
	{
		p.loadPixels();
		for(int i = 0; i < p.pixels.length; i++ )
		{
			p.pixels[i] = (int)p.random(0, 500);
		}
		p.updatePixels();
	}
	
	public void draw()
	{
		/*
		p.myCam.reverseTransform();
		
		p.fill(255);
		p.ellipse(0, 0, 100, 100);
		p.image(myStatic, 0, 0);
		p.noFill();
		
		p.myCam.feed();
		*/
	}
	
}
