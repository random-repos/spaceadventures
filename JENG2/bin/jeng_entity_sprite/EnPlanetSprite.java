package jeng_entity_sprite;

import java.util.LinkedList;
import java.util.List;

import jeng_container.Container;
import jeng_entity.EnSprite;
import jeng_entity_planet.EnPlanet;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;

public class EnPlanetSprite extends EnSprite{
	//for planet mode
	PairFloat vel = new PairFloat(0,0);
	boolean planetMode = true;
	boolean inAir = false;
	int RADIUS = 67;
	double GCONST = 500000;
	double rotAngle = 0;
	float timeRes = 1/50; //resolution of approximation in ms
	List<EnPlanet> planetList = new LinkedList<EnPlanet>();
		
	//container variables
	Container myCont;
	
	public EnPlanetSprite(MainGame ap)
	{
		super(ap, "testxml.xml", "sprite");
		
		//set up container with an arrow
		myCont = new Container(this,ap);
		
		EnArrow tempEntity = new EnArrow(ap, 0, "HOLD");
		myCont.addEntity(tempEntity);
		
		vel.y -= -1300;
	}
	
}
