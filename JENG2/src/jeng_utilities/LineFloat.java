package jeng_utilities;

public class LineFloat {
	public PairFloat s, e;
	public LineFloat()
	{
		s = new PairFloat(0,0);
		e = new PairFloat(0,0);
	};
	public LineFloat(float sx, float sy, float ex, float ey)
	{ 
		s = new PairFloat(sx,sy);
		e = new PairFloat(ex,ey);
	};
	
	public void translate(float x, float y)
	{
		s.x += x;
		e.x += x;

		s.y += y;
		e.y += y;
	}
	
	public void move(float x, float y)
	{
		s.x = x;
		e.x = x;

		s.y = y;
		e.y = y;
	}
	
	
}
