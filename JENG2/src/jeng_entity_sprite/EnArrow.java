package jeng_entity_sprite;

import processing.core.*;
import jeng_entity.EnHit;
import jeng_entity.EnSprite;
import jeng_game.*;

public class EnArrow extends EnSprite 
{
	EnSprite parentSprite;
	
	//are coordin ates absolute or relative to parent?
	public boolean drawAbs = false;
	boolean isVisible = true;
	public void toggle() { isVisible = !isVisible; }
	
	double direction;
	
	//atype can be hold or tap
	//direction is direction in radians 
	public EnArrow(MainGame ap, double adirection, String atype)
	{
		super(ap, "arrow.xml", "sprite");
		myState = atype;
		direction = adirection;
		
		pos.x = 0;
		pos.y = -100;
	}
	

	public void update()
	{		
		
		if(myFrame.id == 11 && ((EnHit)p.mySprite).pos.x > pos.x + 300 )
		{
			isVisible = false;
		}
		time.update();
	}
	
	public void draw()
	{
		//grab image data
		myFrame = mySg.grabFrame(myState);
		
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		p.imageMode(PApplet.CENTER);
		
		
		//set tint
		//check if on tinting frame
		if(myFrame.id == 1)
		{
			p.tint(255, 255 * mySg.getTimeSinceLastUpdate()/myFrame.time);
		}
		if(myFrame.id == 9 || myFrame.id == 11)
		{
			p.tint(255, 100 -  255 * mySg.getTimeSinceLastUpdate()/myFrame.time);
		}
		
		float drawX = pos.x;
		float drawY = pos.y;
		
		//rotate
		p.pushMatrix();
		
		p.rotateAt((float)direction, drawX, drawY);
		
		p.smooth();
		//draw the image
		if(isVisible)
			p.image( tempImage, drawX, drawY );
		
		p.noSmooth();
		
		//return matrix to original
		p.popMatrix();		
		//turn of tinting
		p.noTint();
		
		p.imageMode(PApplet.CORNERS);
		
		
	}

}
