package jeng_entity_honey;
import java.util.LinkedList;
import java.util.List;

import processing.core.*;
import jeng_container.Message;
import jeng_entity.*;
import jeng_game.*;
import jeng_utilities.*;
import jeng_entity_planet.*;

public class EnFoodMarket extends Entity {
	
	//info about square size
	int blockSize = 10;
	int gridSpace = 12;
	
	//sell frequency
	//number of ms per sale
	int msPerSale = 30;
	int lastSale = 0;
	

	
	Matrix<Cell> mtx;
	
	PairFloat pos = new PairFloat(0,0);
	
	EnSpriteMoonMan moonMan = null;
	
	Timer time;
	
	//1 is explosion, 2 is conway
	int mode = 1;
	
	boolean isDraw = true;
	
	public EnFoodMarket(MainGame ap)
	{
		super(ap);
		mtx = new Matrix<Cell>(110,90);
		createEmptyCells();
		time = new Timer(p);
		
	}
	
	public String toString() { return "EnFoodMarket"; }
	
	//cps = chances per second
	private void spont(int x, int y, int r, float cps)
	{
		float cpspc = cps/(4*r^2);
		
		for(int j = y-r; j < y+r; j++)
		{
			for(int i = x-r; i < x+r; i++ )
			{
				//check if in bounds
				if(i >= 0 && i < mtx.nWidth && j >= 0 && j < mtx.nLength)
				{
					if(p.random(0,1) < cpspc * time.getSecPast())
					{
						Cell newCell = new Cell(p,mtx,i,j);
						
						mtx.set(i, j, newCell);
						
						newCell.color.alive = true;
						newCell.color.h = (int)p.random(0,255);
						newCell.color.s = (int)p.random(250,255);
						newCell.color.b = (int)p.random(250,255);
					}
				}
			}
		}
		
	}
	
	private void createRandom(int chance)
	{
		for(int j = 0; j < mtx.nLength; j++)
		{
			for(int i = 0; i < mtx.nWidth; i++ )
			{
				if((int)p.random(0,chance) == 1 && mtx.at(i,j) == null)
				{
					Cell newCell = new Cell(p,mtx,i,j);
					
					mtx.set(i, j, newCell);
					
					newCell.color.alive = true;
					newCell.color.h = (int)p.random(0,255);
					newCell.color.s = (int)p.random(240,255);
					newCell.color.b = (int)p.random(240,255);
				}
			}
		}
	}
	
	public void update()
	{
		
	
		//createRandom(50000);
		
		if(time.getTime() - lastSale > msPerSale)
		{
			lastSale = time.getTime();
			if(moonMan != null)
			{
				spont((int)(moonMan.getImageCenter().x - pos.x)/gridSpace, 
					(int)(moonMan.getImageCenter().y-pos.y)/gridSpace, 2, 1);
			}
			
			for(Cell cl : mtx.ls)
			{
				if(cl != null)
					cl.sell();
			}
			for(Cell cl : mtx.ls)
			{
				if(cl != null)
					cl.relax();
			}
			for(Cell cl : mtx.ls)
			{
				if(cl != null)
					cl.evolve();	
			}
			for(int i = 0; i < mtx.size(); i++)
			{
				//if its dead
				if(mtx.ls.get(i) != null && mtx.ls.get(i).color.alive == false)
					mtx.ls.set(i, null);
			}
		}

		
		time.update();
	}
	
	
	public void createRecursive(int x, int y)
	{
		
	}

	private void createEmptyCells()
	{
		for(int i = 0; i < mtx.nWidth * mtx.nLength; i++)
		{			
			mtx.ls.add(null);
		}
	}
	
	public void draw()
	{
	
		p.myCam.reverseTransform();
		//p.myCam.feed((float).5);
		for(int j = 0; j < mtx.nLength; j++)
		{
			for(int i = 0; i < mtx.nWidth; i++ )
			{
				if(mtx.at(i, j) != null && mtx.at(i, j).color.alive)
				{
					p.noStroke();
					p.colorMode(p.HSB);
					p.fill(mtx.at(i, j).color.h,
							mtx.at(i, j).color.s,
							mtx.at(i, j).color.b);
					
					p.rect(pos.x + i*gridSpace, pos.y + j*gridSpace, blockSize, blockSize);
					p.noFill();
					p.colorMode(p.RGB);
				}
			}
		}	
		//p.myCam.reverseTransform();
		p.myCam.feed();
		
	}
	
	private void toggle()
	{
		if(mode == 1)
			mode = 2;
		else
			mode = 1;
		
		for(Cell cl : mtx.ls)
		{
			if(cl != null)
				cl.ruleSet = mode;
		}
	}
	
	public void sendMessage(Message amsg)
	{
		if(amsg.from instanceof EnSpriteMoonMan)
		{			
			if(amsg.msg instanceof EnPlanet)
			{
				
				PApplet.println( ((EnPlanet)amsg.msg).id );
				if(isDraw)
				{
					if( ((EnPlanet)amsg.msg).id == 666 )
					{
						//create lots of explosions
						createRandom(300);
						//die
						isDraw = false;
					}
					else if ( ((EnHit)p.mySprite).pos.x > 3000)
					{
						createRandom(4000);
					}
				}
			}
			else
			{
				moonMan = (EnSpriteMoonMan)amsg.from;
				//toggle mode of all cells
				toggle();
			}
			
			
		}
	}
	
	
}