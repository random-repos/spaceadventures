package jeng_entity_honey;

public class DNA {
	
	public boolean alive = false;
	public int iteration = 0;

	int h = 255;
	int s = 255;
	int b = 255;
	
	public DNA()
	{
		
	}
	
	public DNA(int r_, int g_, int b_)
	{
		h = r_;
		s = g_;
		b = b_;
	}
	
	public DNA(int r_, int g_, int b_, boolean alive_)
	{
		h = r_;
		s = g_;
		b = b_;
		
		alive = alive_;
	}
	
	public DNA copy()
	{
		DNA tempDNA = new DNA(h, s, b, alive);
		tempDNA.iteration = iteration;
		return tempDNA;
	}
	

}
