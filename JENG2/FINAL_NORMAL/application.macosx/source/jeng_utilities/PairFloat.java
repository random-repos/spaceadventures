package jeng_utilities;

public class PairFloat {
	public float x = 0;
	public float y = 0;
	
	public PairFloat()
	{
		
	}
	
	public PairFloat(float ax, float ay)
	{
		x = ax;
		y = ay;
	}
	
	public void setZero()
	{
		x = 0;
		y = 0;
	}
	
	public PairFloat clone()
	{
		return new PairFloat(x,y);
	}
}
