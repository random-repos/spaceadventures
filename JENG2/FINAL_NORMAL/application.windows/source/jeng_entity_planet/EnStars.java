package jeng_entity_planet;

import java.util.List;
import java.util.Vector;

import processing.core.PApplet;

import jeng_entity.EnHit;
import jeng_entity.Entity;
import jeng_game.MainGame;
import jeng_utilities.PairInt;
import jeng_utilities.Timer;

public class EnStars extends Entity {

	Timer time;
	
	boolean isDraw = false;
	boolean isFade = false;
	public float tintPercent = 0;
	
	//randoms a number this big, if it is less than one, than it creates a star
	int chance = 2000;
	
	int layers = 4;
	double depth[] = {.1,.3,.6,.9};
	double density[] = { 2, 1.8, 1.6, 1.4 };
	int size[] = {1,1,2,2};
	int tint[] = {210,220,230,255};
	List<List<PairInt>> starList = new Vector<List<PairInt>>();
	
	public EnStars(MainGame ap)
	{
		super(ap);
		time = new Timer(ap);
		
		for(int i = 0; i < layers; i++)
		{
			Vector<PairInt> tempList = new Vector<PairInt>();
			
			//go through each pixel
			for(int r = 0; r < p.screen.width; r++ )
			{
				for(int c = 0; c < p.screen.height; c++)
				{
					//random chance
					if( p.random((float)(chance*density[i])) <= 1 )
					{
						tempList.add(new PairInt(r,c));
					}
				}
			}
			
			starList.add(tempList);
		}
	}
	
	public String toString() { return "EnStars"; }
	
	public void update()
	{
		//set display tint
		if( ((EnHit)p.mySprite).pos.x > 1500 && ((EnHit)p.mySprite).pos.x < 4500 && !isFade)
		{
			isDraw = true;
			if(PApplet.lerp( 0, 1, (((EnHit)p.mySprite).pos.x-1500)/3000 ) > tintPercent)
				tintPercent = PApplet.lerp( 0, 1, (((EnHit)p.mySprite).pos.x-1500)/3000 );
		}
		
		if(isFade)
		{
			if(tintPercent > 0)
			{
				tintPercent -= (float)time.getSecPast()/10;
			}
		}
		
		time.update();
	}
	
	public void draw()
	{
		if(isDraw)
		{
			//for each layer
			for(int i = 0; i < layers; i++)
			{
				p.myCam.reverseTransform();
				
				p.fill(tint[i]*tintPercent);
				
				
				//calculate offset
				int offsetX = (int)(p.myCam.pos.x * depth[i]);
				int offsetY = -(int)(p.myCam.pos.y * depth[i]);
				offsetX %= p.screen.width;
				offsetY %= p.screen.height;
				
				for(PairInt place : starList.get(i))
				{
					int drawX = (place.x + offsetX ) % p.screen.width;
					int drawY = (place.y + offsetY ) % p.screen.height;
					
					p.rect(p.screen.width - drawX, drawY, size[i], size[i]);
					
				}
				
				p.noFill();
				
				p.myCam.feed();
			}
		}
	}
}
