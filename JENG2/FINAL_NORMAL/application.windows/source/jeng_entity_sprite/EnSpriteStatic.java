package jeng_entity_sprite;

import processing.core.PImage;
import jeng_entity.EnSprite;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;

public class EnSpriteStatic extends EnSprite{

	float zoomRatio = 1;
	public EnSpriteStatic(MainGame ap, String filename)
	{
		super(ap, filename, "sprite");		
	}
	
	public EnSpriteStatic(MainGame ap, String filename, float azoomRatio)
	{
		super(ap, filename, "sprite");		
		zoomRatio = azoomRatio;
	}
	
	public String toString() { return "EnSpriteStatic"; }
	
	public void update()
	{
		time.update();
	}
	public void draw()
	{
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
	
		p.fill(255);
		
		if(zoomRatio == 1)
			p.image( tempImage, pos.x, pos.y );
		else
		{
			p.myCam.reverseTransform();
			p.myCam.feed(zoomRatio);
			p.image( tempImage, pos.x, pos.y );
			p.myCam.reverseTransform();
			p.myCam.feed();
		}
		
		p.noFill();
	}
}
