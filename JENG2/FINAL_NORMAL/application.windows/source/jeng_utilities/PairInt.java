package jeng_utilities;

public class PairInt {
	public int x = 0;
	public int y = 0;
	
	public PairInt()
	{
		
	}
	
	public PairInt(int ax, int ay)
	{
		x = ax;
		y = ay;
	}
	
	public void setZero()
	{
		x = 0;
		y = 0;
	}
	
	public PairInt clone()
	{
		return new PairInt(x,y);
	}
}
