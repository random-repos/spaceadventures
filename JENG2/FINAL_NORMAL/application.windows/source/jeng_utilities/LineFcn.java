package jeng_utilities;

public class LineFcn {
	
	public static float getSquare(float a)
	{
		return a*a;
	}
	public static float getLength(float x1, float y1, float x2, float y2)
	{
		return (float)Math.sqrt(getSquare(x1-x2) + getSquare(y1-y2));
	}
	public static float getLength(PairFloat a, PairFloat b)
	{
		return getLength(a.x, a.y, b.x, b.y); 
	}
	public static float getLength(PairFloat a)
	{
		return getLength(0,0,a.x,a.y);
	}
	public static float getAngle(float x1, float y1, float x2, float y2)
	{
		return (float)Math.atan2(y2-y1, x2-x1);
	}
	public static float getAngle(PairFloat a, PairFloat b)
	{
		return getAngle(a.x, a.y, b.x, b.y);
	}
}
