package jeng_utilities;

public class Rectangle {
	public float x = 0;
	public float y = 0;
	public float w = 0;
	public float h = 0;
	
	public void set(float ax, float ay, float aw, float ah)
	{
		x = ax;
		y = ay;
		w = aw;
		h = ah;
	}
}
