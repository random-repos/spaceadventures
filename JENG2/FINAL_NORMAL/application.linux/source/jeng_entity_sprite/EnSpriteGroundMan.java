package jeng_entity_sprite;

import processing.core.*;
import jeng_utilities.*;
import jeng_container.Container;
import jeng_container.Message;
import jeng_entity.EnBorder;
import jeng_entity.EnDust;
import jeng_entity.EnSprite;
import jeng_game.MainGame;


public class EnSpriteGroundMan extends EnSprite{
	
	//direction variables
	//direction angle in radians
	double angle = 0;
	// left
	boolean dir = true;
	
	boolean isCollide = false;
	boolean isZafuCollide = false;
	
	//container variables
	Container myCont;
	
	public EnSpriteGroundMan(MainGame ap)
	{
		super(ap, "testxml4.xml", "sprite");
		
		//set up container with an arrow
		myCont = new Container(this,ap);
		
		//EnArrow tempEntity = new EnArrow(ap, 0, "HOLD");
		//myCont.addEntity(tempEntity);
		
		myCont.addEntity(new EnDust(p));
	}
	
	public String toString() { return "EnSpriteGroundMan"; };
	
	private void handleInput()
	{

		myState = "STAND";
		
		if( p.myKeys.left )
		{
			if(dir == false)
			{
				angle = Math.PI;
				myState = "RUN";
			}
			//set left
			dir = false;
		}
		
		if(p.myKeys.right)
		{
			if(dir == true)
			{
				angle = 0;
				myState = "RUN";
			}
			
			dir = true;
		}
		
		//if collision
		if(isCollide)
		{
			if(p.myKeys.down)
			{
				myState = "CROUCH";
				
				//send message to painting
				Message tempMsg = new Message(this);
				tempMsg.toString = "EnPainting";
				msgStack.add(tempMsg);
			}
		}
		else
		{		
			if(p.myKeys.up)
			{
				angle = -Math.PI/2;
				myState = "RUN";
				
				
				if(p.myKeys.right)
					angle = -Math.PI/4;
				else if(p.myKeys.left)
					angle = -3*Math.PI/4;
			}
			
			if(p.myKeys.down)
			{
				angle = Math.PI/2;
				myState = "RUN";
				if(p.myKeys.right)
					angle = Math.PI/4;
				else if(p.myKeys.left)
					angle = 3*Math.PI/4;
			}			
		}
	}

	public void update()
	{
		//handle the input
		handleInput();
		
		//if there is collision, set arrow to visible
		if(isCollide)
		{
			EnArrow tempArrow = (EnArrow)myCont.getEntity("EnArrow");
			if(tempArrow != null)
				tempArrow.isVisible = true;			
		}
		
		//assume there was no collision
		isCollide = false;
		isZafuCollide = false;
		
		//update position like normal
		pos.x += (float)Math.cos((float)angle)*myFrame.velocity*time.getSecPast()/2;
		pos.y += (float)Math.sin((float)angle)*myFrame.velocity*time.getSecPast()/5;
		
		//update child objects
		myCont.update();

		//update timer
		time.update();
	}
	
	public void sendMessage(Message amsg)
	{
		//we gets a message -> collision
		if(amsg.from.toString() == "EnZafu")
		{
			isCollide = true;
			isZafuCollide = true;
		}
		
		if(amsg.from instanceof EnBorder)
		{
			isCollide = true;
		}
	}
	
	public void draw()
	{
		//this should really be in update but who cares
		if(myFrame.id == 6)
		{
			Message tempMsg = new Message(this);
			tempMsg.toString = "EnDust";
			//myCont.sendMessage(tempMsg);
		}
		
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
		
		
		float oldY = pos.y;
		//if zafu collide
		if(isZafuCollide)
			pos.y -= 25;
		
		//tint test
		//p.tint(127, 10);
				
		p.pushMatrix();
		p.smooth();
		p.fill(255);
		//if direction is RIGHT
		if(dir)
		{
			p.image
			(
				tempImage,
				pos.x, 
				pos.y
			);
			
			myCont.draw();
			
			
		} else 
		{
			p.pushMatrix();
			p.scale( (float)-1.0, (float)1.0);
			p.image
			(
				tempImage,
				-pos.x - myFrame.myDrawBox.w, 
				pos.y
			);
			myCont.draw();
			p.popMatrix();
		}
		p.noFill();
		p.noSmooth();
		p.popMatrix();
		
		pos.y = oldY;
		
		//TO DRAW RELATIVE WE SHOULD
		//pushMatrix
		//make image location our origin
		//draw self
		//draw children
		//draw container relative
		//pop matrix
		//draw stuff in container absolute
	}
	
	
	public PairFloat getImageCenter(PairFloat apos)
	{
		return new PairFloat(apos.x + myFrame.myDrawBox.w/2, apos.y + myFrame.myDrawBox.h/2 + 20);
	}
	public PairFloat getImageCenter()
	{
		return new PairFloat(pos.x + myFrame.myDrawBox.w/2, pos.y + myFrame.myDrawBox.h/2 + 20);
	}
	
	public void moveCenter(PairFloat apos)
	{
		pos.x = apos.x - myFrame.myDrawBox.w/2;
		pos.y = apos.y - myFrame.myDrawBox.h/2 + 20;
	}
	
	public Rectangle getHitRect()
	{
		hitRect.x = pos.x+30;
		hitRect.y = pos.y+20;
		hitRect.w = 90;
		hitRect.h = 120;
		return hitRect;
	}
}

