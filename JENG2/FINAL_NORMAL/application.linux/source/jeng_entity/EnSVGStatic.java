package jeng_entity;

import jeng_game.MainGame;
import jeng_utilities.Timer;
import processing.core.*;

public class EnSVGStatic extends EnHit {

	public PShape myShape;
	float zoomRatio = 1;
	public boolean move = false;
	Timer time;
	
	public EnSVGStatic(MainGame ap, String filename)
	{
		super(ap);
		myShape = p.loadShape(filename);
		time = new Timer(p);
	}
	
	public EnSVGStatic(MainGame ap, String filename, float azoomRatio)
	{
		super(ap);
		myShape = p.loadShape(filename);
		zoomRatio = azoomRatio;
		time = new Timer(p);
	}
	
	public String toString() { return "EnSVGStatic"; }
	
	public void update()
	{
		if(move)
		{
			myShape.translate(75*time.getSecPast(),50*time.getSecPast());
		}
		time.update();
	}
	
	public void draw()
	{
		if(zoomRatio == 1)
			p.shape(myShape,pos.x, pos.y, myShape.width, myShape.height);
		else
		{
			p.myCam.reverseTransform();
			p.myCam.feed(zoomRatio);
			p.smooth();
			
			p.shapeMode(PApplet.CENTER);
			p.shape(myShape,pos.x, pos.y, myShape.width, myShape.height);
			p.shapeMode(PApplet.CORNERS);
			
			p.noSmooth();
			p.myCam.reverseTransform();
			p.myCam.feed();
		}
	}
	
	//get hit box
	//should return PShape hitbox
}
